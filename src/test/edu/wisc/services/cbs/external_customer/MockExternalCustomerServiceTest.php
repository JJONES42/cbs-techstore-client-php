<?php

use edu\wisc\services\cbs\external_customer\ExternalCustomer;
use edu\wisc\services\cbs\external_customer\ExternalCustomerServiceResponse;
use edu\wisc\services\cbs\external_customer\MockExternalCustomerService;

/**
 * Tests for {@link MockExternalCustomerService}
 */
class MockExternalCustomerServiceTest extends \PHPUnit\Framework\TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockExternalCustomerService();
        $customer = new ExternalCustomer();
        $response = $client->createCustomer($customer);
        static::assertTrue($response->isSuccess());
    }

    /** @test */
    public function returnsCustomResponse()
    {
        $mockResponse = new ExternalCustomerServiceResponse(
            true,
            "Custom External Customer Service Response",
            '123456789'
        );
        $client = new MockExternalCustomerService($mockResponse);
        $customer = new ExternalCustomer();
        $response = $client->createCustomer($customer);
        static::assertEquals($mockResponse, $response);
    }

}