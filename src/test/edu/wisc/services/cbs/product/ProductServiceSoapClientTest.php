<?php
namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\product\create\generated\DOIT_SOA_ITEM_CREATE_V4_Service;
use edu\wisc\services\cbs\product\create\generated\OutputParameters;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

/**
 * Unit tests for {@link ProductServiceSoapClient}
 */
class ProductServiceSoapClientTest extends TestCase
{

    /** @var MockInterface */
    private $itemSoapService;

    /** @var SoapProductService */
    private $productService;

    /**
     * Initializes the mock client before each test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->itemSoapService = \Mockery::mock(DOIT_SOA_ITEM_CREATE_V7_Service::class);
        $this->productService = new SoapProductService('', '', $this->itemSoapService);
    }

    /** @test */
    public function successResponseIsCaseInsensitive()
    {
        foreach (['success', 'SUCCESS', 'Success'] as $status) {
            static::assertTrue(
                SoapProductService::createResponse(new OutputParameters($status, '', '', ''))->isSuccess()
            );
        }
    }

    /** @test */
    public function unsuccessfulByDefault()
    {
        foreach ([null, '', 0, 'NOPE'] as $status) {
            static::assertFalse(
                SoapProductService::createResponse(new OutputParameters($status, '', '', ''))->isSuccess()
            );
        }
    }

}
