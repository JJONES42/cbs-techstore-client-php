<?php
namespace edu\wisc\services\cbs\product;

use PHPUnit\Framework\TestCase;

/**
 * Tests for {@link MockProductServiceClient}
 */
class MockProductServiceClientTest extends TestCase
{

    /** @var Product */
    private $product;

    public function setUp()
    {
        $this->product = (new Product())
            ->setItemNumber('85000')
            ->setDescription('ADI IA Test Product')
            ->setCbsItemTemplate('Commodity')
            ->setItemCategory('COMP/APPLE/DESKTOP')
            ->setManufacturer('Hoffmann Manufacturers')
            ->setManufacturerPartNumber("MFG12345")
            ->setCost(5)
            ->setMinQuantity(1)
            ->setMaxQuantity(5)
            ->setLifecycle('SELLABLE_ONLY')
            ->setInventoryControlType('STOCK')
            ->setSerialControlFlag('None')
            ->setInventoryControlType('STOCK')
            ->setVendor('B&H Photo')
            ->setVendorPartNumber("VNDR12345")
            ->setUpc(uniqid('', false))
            ->setBasePrice(5)
            ->setPricingTemplate('1')
            ->setReference('ref')
            ->setCostCenter('5056')
            ->setPlannerCode('PM-JAM')
            ->setMinMaxActive('no')
            ->setBuyer('NONE')
            ->setProductManager('Chris Last')
            ->setTestMode('yesplz');
    }

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockProductService();
        $response = $client->createProduct($this->product);
        static::assertTrue($response->isSuccess());
    }

}
