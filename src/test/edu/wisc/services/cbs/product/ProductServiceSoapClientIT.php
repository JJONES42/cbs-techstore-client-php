<?php

namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\IntegrationTestCase;
use Money\Money;

/**
 * Integration tests for {@link ProductServiceSoapClient}
 */
class ProductServiceSoapClientIT extends IntegrationTestCase
{

    /** @var  SoapProductService */
    private $client;

    public function setUp()
    {
        $this->client = new SoapProductService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
    }

    public function testCreateAndUpdateProduct()
    {
        // First create the product so that it exists in the first place
        $product = $this->generateSampleProduct();
        $response = $this->client->createProduct($product);
        static::assertTrue($response->isSuccess(), $response->getMessage());

        // Then update it
        $product->setCost(Money::USD(100));
        $response = $this->client->updateProduct($product);
        static::assertTrue($response->isSuccess(), $response->getMessage());
    }

}
