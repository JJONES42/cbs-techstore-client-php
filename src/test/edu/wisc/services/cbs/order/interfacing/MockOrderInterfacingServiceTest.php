<?php

use edu\wisc\services\cbs\order\interfacing\MockOrderInterfacingService;
use edu\wisc\services\cbs\order\OrderServiceResponse;
use PHPUnit\Framework\TestCase;

class MockOrderInterfacingServiceTest extends TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockOrderInterfacingService();
        $response = $client->interfaceOrder("123456");
        static::assertTrue($response->isSuccess());
    }

}
