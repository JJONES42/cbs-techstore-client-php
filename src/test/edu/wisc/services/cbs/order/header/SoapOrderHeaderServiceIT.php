<?php

use \edu\wisc\services\cbs\api\SoapService;
use \edu\wisc\services\cbs\order\header\SoapOrderHeaderService;
use \edu\wisc\services\cbs\IntegrationTestCase;

/**
 * Integration tests to test the various methods of instantiating a {@link SoapOrderHeaderService}.
 * A {@link SoapOrderHeaderService} can be instantiated using:
 *
 * 1. Only username and password
 * 2. Username, password and WSDL to specify a CBS environment
 * 3. Username, password and instance of {@link MockOrderHeaderServiceClient}
 */
class SoapOrderHeaderServiceIT extends IntegrationTestCase
{

    /**
     * @test
     */
    public function createsClientWithOnlyUsernameAndPassword()
    {
        $soapOrderHeaderService = new SoapOrderHeaderService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        static::assertNotNull($soapOrderHeaderService);
    }

    /**
     * @test
     */
    public function createsClientWithWsdl()
    {
        $soapOrderHeaderService = new SoapOrderHeaderService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password'],
            SoapOrderHeaderService::CBQA12
        );
        static::assertNotNull($soapOrderHeaderService);
    }

}