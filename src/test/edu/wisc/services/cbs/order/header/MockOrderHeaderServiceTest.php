<?php

use edu\wisc\services\cbs\order\header\MockOrderHeaderService;
use edu\wisc\services\cbs\order\header\OrderHeader;
use edu\wisc\services\cbs\order\OrderServiceResponse;

class MockOrderHeaderServiceTest extends \PHPUnit\Framework\TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockOrderHeaderService();
        $header = new OrderHeader();
        $response = $client->createOrderHeader($header);
        static::assertTrue($response->isSuccess());
    }

}

