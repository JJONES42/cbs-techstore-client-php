<?php

use edu\wisc\services\cbs\IntegrationTestCase;
use edu\wisc\services\cbs\order\header\OrderHeader;
use edu\wisc\services\cbs\order\header\SoapOrderHeaderService;
use edu\wisc\services\cbs\order\interfacing\SoapOrderInterfacingService;
use edu\wisc\services\cbs\order\line\OrderLine;
use edu\wisc\services\cbs\order\line\SoapOrderLineService;
use edu\wisc\services\cbs\order\payment\OrderPayment;
use edu\wisc\services\cbs\order\payment\OrderPaymentServiceResponse;
use edu\wisc\services\cbs\order\payment\SoapOrderPaymentService;
use Money\Money;

/**
 * Integration test to simulate the full CBS order integration process for an order. This includes:
 *
 * 1. Creating an {@link OrderPayment}
 * 2. Creating an {@link OrderHeader}
 * 3. Creating all {@link OrderLine}s
 * 4. Interfacing the order with CBS
 */
class CreateCbsOrderIT extends IntegrationTestCase
{

    /** @var SoapOrderHeaderService */
    private $soapHeaderClient;

    /** @var SoapOrderLineService */
    private $soapLineClient;

    /** @var SoapOrderPaymentService */
    private $soapPaymentClient;

    /** @var SoapOrderInterfacingService */
    private $soapInterfacingClient;

    /** @var int randomly generated order number */
    protected $orderNumber;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->soapHeaderClient = new SoapOrderHeaderService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->soapLineClient = new SoapOrderLineService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->soapPaymentClient = new SoapOrderPaymentService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->soapInterfacingClient = new SoapOrderInterfacingService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->orderNumber = mt_rand(90000000, 99999999);
    }

    /**
     * @test
     */
    public function createsCbsOrder()
    {
        /** @var OrderHeader $header */
        $header = $this->generateSampleOrderHeader($this->orderNumber);
        /** @var OrderLine $lineItem */
        $lineItem = $this->generateSampleOrderLine($this->orderNumber);
        /** @var OrderPayment $payment */
        $payment = $this->generateSampleOrderPayment($this->orderNumber);

        /** @var OrderPaymentServiceResponse $paymentResponse */
        $paymentResponse = $this->soapPaymentClient->createOrderPayment($payment);
        $headerResponse = $this->soapHeaderClient->createOrderHeader($header);
        $lineResponse = $this->soapLineClient->createOrderLine($lineItem);
        $interfacingResponse = $this->soapInterfacingClient->interfaceOrder($this->orderNumber);

        static::assertTrue($paymentResponse->isSuccess(), $paymentResponse->getMessage());
        static::assertTrue($headerResponse->isSuccess(), $headerResponse->getMessage());
        static::assertTrue($lineResponse->isSuccess(), $lineResponse->getMessage());
        static::assertTrue($interfacingResponse->isSuccess(), $interfacingResponse->getMessage());

        static::assertEquals(Money::USD(0), $paymentResponse->getBalanceDue());
    }

}
