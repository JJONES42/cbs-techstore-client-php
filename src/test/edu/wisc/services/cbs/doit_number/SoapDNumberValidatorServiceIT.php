<?php

namespace edu\wisc\services\cbs\doit_number;

use edu\wisc\services\cbs\IntegrationTestCase;

/**
 * Integration tests for {@link SoapDNumberValidatorService}.
 */
class SoapDNumberValidatorServiceIT extends IntegrationTestCase
{

    /** @var SoapDNumberValidatorService */
    private $client;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->client = new SoapDNumberValidatorService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
    }

    /**
     * @test
     */
    public function failureWithInvalidDNumberAndUser()
    {
        $response = $this->client->validateDNumber("D000229", 'DEPARTMENTAL', 'UW123A456', 'PVI');
        self::assertFalse($response->isSuccess(), $response->getMessage());
    }

}
