<?php

namespace edu\wisc\services\cbs\price;

use PHPUnit\Framework\TestCase;

class MockPriceServiceClientTest extends TestCase
{

    /** @test */
    public function alwaysSucceedsByDefault()
    {
        $client = new MockPriceService();
        $response = $client->updateProductPrice("skusku", 42.00);
        static::assertTrue($response->isSuccess());
    }
    
    /** @test */
    public function returnsFailureResponse()
    {
        $client = new MockPriceService(false);
        $response = $client->updateProductPrice("skusku", 42.00);
        static::assertFalse($response->isSuccess());
    }
    
}

