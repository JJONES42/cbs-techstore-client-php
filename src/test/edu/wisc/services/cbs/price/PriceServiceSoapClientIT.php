<?php
namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\product\Product;
use edu\wisc\services\cbs\IntegrationTestCase;
use edu\wisc\services\cbs\product\ProductService;
use edu\wisc\services\cbs\product\SoapProductService;
use Money\Money;

class PriceServiceSoapClientIT extends IntegrationTestCase
{
    /** @var  SoapPriceService */
    private $client;

    /** @var Product */
    private $product;

    /** @var ProductService */
    private $productService;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->client = new SoapPriceService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->productService = new SoapProductService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
        $this->product = $this->generateSampleProduct();
        $this->product->setItemNumber("CBSPRICESVCSOAP");
        $this->product->setDescription("CBS Price Service SOAP Client IT");
        $this->productService->createProduct($this->product);
    }

    /** @test */
    public function updatesProductPrice()
    {
        $price = mt_rand(1, 1000000) / 100;
        $response = $this->client->updateProductPrice($this->product->getItemNumber(), $price);
        static::assertTrue($response->isSuccess(), $response->getMessage());
    }
}
