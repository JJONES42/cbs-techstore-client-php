<?php

namespace edu\wisc\services\cbs\item_comp;

use edu\wisc\services\cbs\IntegrationTestCase;

/**
 * Integration tests for {@link SoapItemCompareService}.
 */
class SoapItemCompareServiceIT extends IntegrationTestCase
{

    /** @var SoapItemCompareService */
    private $client;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->client = new SoapItemCompareService(
            static::$itData['cbs.username'],
            static::$itData['cbs.password']
        );
    }

    /**
     * @test
     */
    public function returnsCbsResult()
    {
        $response = $this->client->compareItemWithCbs("75083");
        self::assertNotNull($response->getCbsResult());
    }

}
