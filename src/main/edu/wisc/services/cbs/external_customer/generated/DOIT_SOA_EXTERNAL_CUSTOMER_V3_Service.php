<?php

namespace edu\wisc\services\cbs\external_customer\generated;

class DOIT_SOA_EXTERNAL_CUSTOMER_V3_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\external_customer\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\external_customer\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\external_customer\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_external_customer_v3.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function CREATE_CUSTOMER(InputParameters $body)
    {
      return $this->__soapCall('CREATE_CUSTOMER', array($body));
    }

}
