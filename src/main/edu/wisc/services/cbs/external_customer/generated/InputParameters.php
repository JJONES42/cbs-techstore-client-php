<?php

namespace edu\wisc\services\cbs\external_customer\generated;

class InputParameters
{

    /**
     * @var string $P_CUSTOMER_TYPE
     */
    protected $P_CUSTOMER_TYPE = null;

    /**
     * @var string $P_CUSTOMER_IDENTIFIER
     */
    protected $P_CUSTOMER_IDENTIFIER = null;

    /**
     * @var string $P_FIRST_NAME
     */
    protected $P_FIRST_NAME = null;

    /**
     * @var string $P_LAST_NAME
     */
    protected $P_LAST_NAME = null;

    /**
     * @var string $P_EMAIL
     */
    protected $P_EMAIL = null;

    /**
     * @var string $P_PHONE
     */
    protected $P_PHONE = null;

    /**
     * @var string $P_REFERENCE
     */
    protected $P_REFERENCE = null;

    /**
     * @var string $P_STUDENT_FIRST_NAME
     */
    protected $P_STUDENT_FIRST_NAME = null;

    /**
     * @var string $P_STUDENT_LAST_NAME
     */
    protected $P_STUDENT_LAST_NAME = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param string $P_CUSTOMER_TYPE
     * @param string $P_CUSTOMER_IDENTIFIER
     * @param string $P_FIRST_NAME
     * @param string $P_LAST_NAME
     * @param string $P_EMAIL
     * @param string $P_PHONE
     * @param string $P_REFERENCE
     * @param string $P_STUDENT_FIRST_NAME
     * @param string $P_STUDENT_LAST_NAME
     * @param string $P_TEST_MODE
     */
    public function __construct($P_CUSTOMER_TYPE, $P_CUSTOMER_IDENTIFIER, $P_FIRST_NAME, $P_LAST_NAME, $P_EMAIL, $P_PHONE, $P_REFERENCE, $P_STUDENT_FIRST_NAME, $P_STUDENT_LAST_NAME, $P_TEST_MODE)
    {
      $this->P_CUSTOMER_TYPE = $P_CUSTOMER_TYPE;
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      $this->P_FIRST_NAME = $P_FIRST_NAME;
      $this->P_LAST_NAME = $P_LAST_NAME;
      $this->P_EMAIL = $P_EMAIL;
      $this->P_PHONE = $P_PHONE;
      $this->P_REFERENCE = $P_REFERENCE;
      $this->P_STUDENT_FIRST_NAME = $P_STUDENT_FIRST_NAME;
      $this->P_STUDENT_LAST_NAME = $P_STUDENT_LAST_NAME;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_TYPE()
    {
      return $this->P_CUSTOMER_TYPE;
    }

    /**
     * @param string $P_CUSTOMER_TYPE
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_CUSTOMER_TYPE($P_CUSTOMER_TYPE)
    {
      $this->P_CUSTOMER_TYPE = $P_CUSTOMER_TYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_IDENTIFIER()
    {
      return $this->P_CUSTOMER_IDENTIFIER;
    }

    /**
     * @param string $P_CUSTOMER_IDENTIFIER
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_CUSTOMER_IDENTIFIER($P_CUSTOMER_IDENTIFIER)
    {
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_FIRST_NAME()
    {
      return $this->P_FIRST_NAME;
    }

    /**
     * @param string $P_FIRST_NAME
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_FIRST_NAME($P_FIRST_NAME)
    {
      $this->P_FIRST_NAME = $P_FIRST_NAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_LAST_NAME()
    {
      return $this->P_LAST_NAME;
    }

    /**
     * @param string $P_LAST_NAME
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_LAST_NAME($P_LAST_NAME)
    {
      $this->P_LAST_NAME = $P_LAST_NAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_EMAIL()
    {
      return $this->P_EMAIL;
    }

    /**
     * @param string $P_EMAIL
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_EMAIL($P_EMAIL)
    {
      $this->P_EMAIL = $P_EMAIL;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PHONE()
    {
      return $this->P_PHONE;
    }

    /**
     * @param string $P_PHONE
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_PHONE($P_PHONE)
    {
      $this->P_PHONE = $P_PHONE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_REFERENCE()
    {
      return $this->P_REFERENCE;
    }

    /**
     * @param string $P_REFERENCE
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_REFERENCE($P_REFERENCE)
    {
      $this->P_REFERENCE = $P_REFERENCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_STUDENT_FIRST_NAME()
    {
      return $this->P_STUDENT_FIRST_NAME;
    }

    /**
     * @param string $P_STUDENT_FIRST_NAME
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_STUDENT_FIRST_NAME($P_STUDENT_FIRST_NAME)
    {
      $this->P_STUDENT_FIRST_NAME = $P_STUDENT_FIRST_NAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_STUDENT_LAST_NAME()
    {
      return $this->P_STUDENT_LAST_NAME;
    }

    /**
     * @param string $P_STUDENT_LAST_NAME
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_STUDENT_LAST_NAME($P_STUDENT_LAST_NAME)
    {
      $this->P_STUDENT_LAST_NAME = $P_STUDENT_LAST_NAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\external_customer\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
