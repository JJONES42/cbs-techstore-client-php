<?php

namespace edu\wisc\services\cbs\external_customer;

/**
 * Service intended to create external customers (e.g. Parents) in CBS
 */
interface ExternalCustomerService
{

    /**
     * @param ExternalCustomer $customer
     * @return mixed
     */
    public function createCustomer(ExternalCustomer $customer);

}