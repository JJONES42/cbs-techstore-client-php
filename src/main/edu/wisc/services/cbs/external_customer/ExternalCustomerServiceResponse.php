<?php

namespace edu\wisc\services\cbs\external_customer;

use edu\wisc\services\cbs\common\AbstractServiceResponse;

/**
 * Represents a response from a {@link ExternalCustomerService}
 */
class ExternalCustomerServiceResponse extends AbstractServiceResponse
{

    /** @var string */
    private $customerNumber;

    /** @var string */
    private $status;

    /** @var string */
    private $resultMessage;

    public function __construct($status, $resultMessage, $customerNumber)
    {
        parent::__construct($status, $resultMessage);
        $this->customerNumber = $customerNumber;
    }

    /**
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getResultMessage()
    {
        return $this->resultMessage;
    }

}