<?php

namespace edu\wisc\services\cbs\doit_number\generated;

class DOIT_SOA_DOIT_NUMBER_V1_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\doit_number\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\doit_number\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\doit_number\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_doit_number_v1.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function USER_CHECK(InputParameters $body)
    {
      return $this->__soapCall('USER_CHECK', array($body));
    }

}
