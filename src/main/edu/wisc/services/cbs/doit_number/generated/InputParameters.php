<?php

namespace edu\wisc\services\cbs\doit_number\generated;

class InputParameters
{

    /**
     * @var string $P_DOIT_NUMBER
     */
    protected $P_DOIT_NUMBER = null;

    /**
     * @var string $P_CUSTOMER_TYPE
     */
    protected $P_CUSTOMER_TYPE = null;

    /**
     * @var string $P_CUSTOMER_IDENTIFIER
     */
    protected $P_CUSTOMER_IDENTIFIER = null;

    /**
     * @var string $P_CUSTOMER_IDENTIFIER_TYPE
     */
    protected $P_CUSTOMER_IDENTIFIER_TYPE = null;

    /**
     * @param string $P_DOIT_NUMBER
     * @param string $P_CUSTOMER_TYPE
     * @param string $P_CUSTOMER_IDENTIFIER
     * @param string $P_CUSTOMER_IDENTIFIER_TYPE
     */
    public function __construct($P_DOIT_NUMBER, $P_CUSTOMER_TYPE, $P_CUSTOMER_IDENTIFIER, $P_CUSTOMER_IDENTIFIER_TYPE)
    {
      $this->P_DOIT_NUMBER = $P_DOIT_NUMBER;
      $this->P_CUSTOMER_TYPE = $P_CUSTOMER_TYPE;
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      $this->P_CUSTOMER_IDENTIFIER_TYPE = $P_CUSTOMER_IDENTIFIER_TYPE;
    }

    /**
     * @return string
     */
    public function getP_DOIT_NUMBER()
    {
      return $this->P_DOIT_NUMBER;
    }

    /**
     * @param string $P_DOIT_NUMBER
     * @return \edu\wisc\services\cbs\doit_number\generated\InputParameters
     */
    public function setP_DOIT_NUMBER($P_DOIT_NUMBER)
    {
      $this->P_DOIT_NUMBER = $P_DOIT_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_TYPE()
    {
      return $this->P_CUSTOMER_TYPE;
    }

    /**
     * @param string $P_CUSTOMER_TYPE
     * @return \edu\wisc\services\cbs\doit_number\generated\InputParameters
     */
    public function setP_CUSTOMER_TYPE($P_CUSTOMER_TYPE)
    {
      $this->P_CUSTOMER_TYPE = $P_CUSTOMER_TYPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_IDENTIFIER()
    {
      return $this->P_CUSTOMER_IDENTIFIER;
    }

    /**
     * @param string $P_CUSTOMER_IDENTIFIER
     * @return \edu\wisc\services\cbs\doit_number\generated\InputParameters
     */
    public function setP_CUSTOMER_IDENTIFIER($P_CUSTOMER_IDENTIFIER)
    {
      $this->P_CUSTOMER_IDENTIFIER = $P_CUSTOMER_IDENTIFIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CUSTOMER_IDENTIFIER_TYPE()
    {
      return $this->P_CUSTOMER_IDENTIFIER_TYPE;
    }

    /**
     * @param string $P_CUSTOMER_IDENTIFIER_TYPE
     * @return \edu\wisc\services\cbs\doit_number\generated\InputParameters
     */
    public function setP_CUSTOMER_IDENTIFIER_TYPE($P_CUSTOMER_IDENTIFIER_TYPE)
    {
      $this->P_CUSTOMER_IDENTIFIER_TYPE = $P_CUSTOMER_IDENTIFIER_TYPE;
      return $this;
    }

}
