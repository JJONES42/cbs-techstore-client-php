<?php


 function autoload_7e16fc3f8c55cfa144d60af148b68340($class)
{
    $classes = array(
        'edu\wisc\services\cbs\doit_number\generated\DOIT_SOA_DOIT_NUMBER_V1_Service' => __DIR__ .'/DOIT_SOA_DOIT_NUMBER_V1_Service.php',
        'edu\wisc\services\cbs\doit_number\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\doit_number\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\doit_number\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_7e16fc3f8c55cfa144d60af148b68340');

// Do nothing. The rest is just leftovers from the code generation.
{
}
