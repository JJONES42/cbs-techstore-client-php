<?php

namespace edu\wisc\services\cbs\doit_number;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\doit_number\generated\DOIT_SOA_DOIT_NUMBER_V1_Service;
use edu\wisc\services\cbs\doit_number\generated\InputParameters;
use edu\wisc\services\cbs\doit_number\generated\OutputParameters;

/**
 * SOAP implementation of {@link DNumberValidatorService}.
 */
class SoapDNumberValidatorService implements DNumberValidatorService, SoapService
{

    /** URL for QA WSDL */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_doit_number_v1/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_doit_number_v1/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_doit_number_v1/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_doit_number_v1/?wsdl';

    private $soapClient;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        $username,
        $password,
        $wsdlPath = null,
        \SoapClient $soapClient = null
    ) {
        if ($soapClient !== null) {
            $this->soapClient = $soapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_DOIT_NUMBER_V1_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_DOIT_NUMBER_V1_Service(
                [],
                __DIR__ . '/../../../../../resources/doit_soa_doit_number_v1.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateDNumber($dnumber, $customerType, $customerIdentifier, $customerIdentifierType)
    {
        /** @var InputParameters $inputParameters */
        $inputParameters = new InputParameters(
            $dnumber,
            $customerType,
            $customerIdentifier,
            $customerIdentifierType
        );
        /** @var OutputParameters $outputParameters */
        $outputParameters = $this->soapClient->USER_CHECK($inputParameters);
        $result = json_decode($outputParameters->getP_RESULT_STRING(), true);

        return new DNumberValidatorServiceResponse(
            strcasecmp($result['valid'], "yes") == 0,
            $result['reason']
        );
    }

}