<?php
namespace edu\wisc\services\cbs\order;

use edu\wisc\services\cbs\common\AbstractServiceResponse;

/**
 * The status of a call to an {@link OrderService} class.
 */
class OrderServiceResponse extends AbstractServiceResponse
{
}
