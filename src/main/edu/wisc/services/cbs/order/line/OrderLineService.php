<?php
namespace edu\wisc\services\cbs\order\line;

use edu\wisc\services\cbs\api\Service;
use edu\wisc\services\cbs\order\OrderServiceResponse;

interface OrderLineService extends Service
{

    /**
     * Creates a {@link OrderLine} in CBS.
     *
     * @param OrderLine $orderLine
     * @return OrderServiceResponse
     */
    public function createOrderLine(OrderLine $orderLine): OrderServiceResponse;

}
