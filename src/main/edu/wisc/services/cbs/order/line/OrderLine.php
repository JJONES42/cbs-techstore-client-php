<?php
namespace edu\wisc\services\cbs\order\line;

use edu\wisc\services\cbs\common\Order;

/**
 * A line item in an order.
 */
class OrderLine extends Order
{

    /** @var float  order number - foreign key {@link OrderHeader::getOrderNumber} */
    private $orderNumber;

    /** @var string "MAGE" + order number */
    private $origSysDocumentRef;

    /** @var string  always STANDARD */
    private $lineType = 'STANDARD';

    /** @var float  the sequence in which to be placed in the order */
    private $lineNumber;

    /** @var string Format "MAGE" + Magento order number + "-" + $lineNumber */
    private $origSysLineRef;

    /** @var string  should always be item_number */
    private $itemNumberType = 'item_number';

    /** @var string  SKU */
    private $itemNumber;

    /** @var float  quantity purchased */
    private $quantity;

    /** @var float  price per unit */
    private $unitPrice;

    /** @var float discount price */
    private $unitPromoPrice;

    /** @var string name of discount */
    private $promoDiscountName;

    /** @var float  (optional) */
    private $bundleId;

    /** @var float  (optional) */
    private $bundleLineId;

    /** @var string */
    private $deliveryMethod;

    /** @var \DateTime  date shipped (optional) */
    private $shipDate;

    /** @var string  ID of person shipped (optional) */
    private $shipToIdentifier;

    /** @var string  type of ID for shipToIdentifier (eg. PVI) (optional) */
    private $shipToIdentifierType;

    /** @var string  shipping address line 1 */
    private $shipToAddress1;

    /** @var string  shipping address line 2 (optional) */
    private $shipToAddress2;

    /** @var string  shipping address line 3 (optional) */
    private $shipToAddress3;

    /** @var string  shipping address line 4 (optional) */
    private $shipToAddress4;

    /** @var string  shipping address city */
    private $shipToCity;

    /** @var string  shipping address state */
    private $shipToState;

    /** @var string  shipping address zip code */
    private $shipToPostalCode;

    /** @var string  shipping instructions (optional) */
    private $shippingInstructions;

    /** @var float  (optional) */
    private $shipSetId;

    /** @var float */
    private $userId;

    /** @var string */
    private $testMode;

    /**
     * @return float
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param float $orderNumber
     * @return OrderLine
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigSysDocumentRef()
    {
        return $this->origSysDocumentRef;
    }

    /**
     * @param string $origSysDocumentRef
     * @return OrderLine
     */
    public function setOrigSysDocumentRef($origSysDocumentRef)
    {
        $this->origSysDocumentRef = $origSysDocumentRef;
        return $this;
    }

    /**
     * @return string
     */
    public function getLineType()
    {
        return $this->lineType;
    }

    /**
     * @param string $lineType
     * @return OrderLine
     */
    public function setLineType($lineType)
    {
        $this->lineType = $lineType;
        return $this;
    }

    /**
     * @return float
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * @param float $lineNumber
     * @return OrderLine
     */
    public function setLineNumber($lineNumber)
    {
        $this->lineNumber = $lineNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigSysLineRef()
    {
        return $this->origSysLineRef;
    }

    /**
     * @param string $origSysLineRef
     * @return OrderLine
     */
    public function setOrigSysLineRef($origSysLineRef)
    {
        $this->origSysLineRef = $origSysLineRef;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemNumberType()
    {
        return $this->itemNumberType;
    }

    /**
     * @param string $itemNumberType
     * @return OrderLine
     */
    public function setItemNumberType($itemNumberType)
    {
        $this->itemNumberType = $itemNumberType;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * @param string $itemNumber
     * @return OrderLine
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return OrderLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return OrderLine
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getBundleId()
    {
        return $this->bundleId;
    }

    /**
     * @param float $bundleId
     * @return OrderLine
     */
    public function setBundleId($bundleId)
    {
        $this->bundleId = $bundleId;
        return $this;
    }

    /**
     * @return float
     */
    public function getBundleLineId()
    {
        return $this->bundleLineId;
    }

    /**
     * @param float $bundleLineId
     * @return OrderLine
     */
    public function setBundleLineId($bundleLineId)
    {
        $this->bundleLineId = $bundleLineId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryMethod()
    {
        return $this->deliveryMethod;
    }

    /**
     * @param string $deliveryMethod
     * @return OrderLine
     */
    public function setDeliveryMethod($deliveryMethod): OrderLine
    {
        $this->deliveryMethod = $deliveryMethod;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getShipDate(): \DateTime
    {
        return $this->shipDate;
    }

    /**
     * @param \DateTime $shipDate
     * @return OrderLine
     */
    public function setShipDate($shipDate)
    {
        $this->shipDate = $shipDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipToIdentifier()
    {
        return $this->shipToIdentifier;
    }

    /**
     * @param string $shipToIdentifier
     * @return OrderLine
     */
    public function setShipToIdentifier($shipToIdentifier)
    {
        $this->shipToIdentifier = $shipToIdentifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipToIdentifierType()
    {
        return $this->shipToIdentifierType;
    }

    /**
     * @param string $shipToIdentifierType
     * @return OrderLine
     */
    public function setShipToIdentifierType($shipToIdentifierType)
    {
        $this->shipToIdentifierType = $shipToIdentifierType;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipToAddress1()
    {
        return $this->shipToAddress1;
    }

    /**
     * @param string $shipToAddress1
     * @return OrderLine
     */
    public function setShipToAddress1($shipToAddress1)
    {
        $this->shipToAddress1 = $shipToAddress1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShipToAddress2()
    {
        return $this->shipToAddress2;
    }

    /**
     * @param string $shipToAddress2
     * @return OrderLine
     */
    public function setShipToAddress2($shipToAddress2)
    {
        $this->shipToAddress2 = $shipToAddress2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShipToAddress3()
    {
        return $this->shipToAddress3;
    }

    /**
     * @param string $shipToAddress3
     * @return OrderLine
     */
    public function setShipToAddress3($shipToAddress3)
    {
        $this->shipToAddress3 = $shipToAddress3;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShipToAddress4()
    {
        return $this->shipToAddress4;
    }

    /**
     * @param string $shipToAddress4
     * @return OrderLine
     */
    public function setShipToAddress4($shipToAddress4)
    {
        $this->shipToAddress4 = $shipToAddress4;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipToCity()
    {
        return $this->shipToCity;
    }

    /**
     * @param string $shipToCity
     * @return OrderLine
     */
    public function setShipToCity($shipToCity)
    {
        $this->shipToCity = $shipToCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipToState()
    {
        return $this->shipToState;
    }

    /**
     * @param string $shipToState
     * @return OrderLine
     */
    public function setShipToState($shipToState)
    {
        $this->shipToState = $shipToState;
        return $this;
    }

    /**
     * @return string
     */
    public function getShipToPostalCode()
    {
        return $this->shipToPostalCode;
    }

    /**
     * @param string $shipToPostalCode
     * @return OrderLine
     */
    public function setShipToPostalCode($shipToPostalCode)
    {
        $this->shipToPostalCode = $shipToPostalCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingInstructions()
    {
        return $this->shippingInstructions;
    }

    /**
     * @param string $shippingInstructions
     * @return OrderLine
     */
    public function setShippingInstructions($shippingInstructions)
    {
        $this->shippingInstructions = $shippingInstructions;
        return $this;
    }

    /**
     * @return float
     */
    public function getShipSetId()
    {
        return $this->shipSetId;
    }

    /**
     * @param float $shipSetId
     * @return OrderLine
     */
    public function setShipSetId($shipSetId)
    {
        $this->shipSetId = $shipSetId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param float $userId
     * @return OrderLine
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param string $testMode
     * @return OrderLine
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getUnitPromoPrice()
    {
        return $this->unitPromoPrice;
    }

    /**
     * @param float $unitPromoPrice
     * @return OrderLine
     */
    public function setUnitPromoPrice($unitPromoPrice)
    {
        $this->unitPromoPrice = $unitPromoPrice;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPromoDiscountName()
    {
        return $this->promoDiscountName;
    }

    /**
     * @param string $promoDiscountName
     * @return OrderLine
     */
    public function setPromoDiscountName($promoDiscountName)
    {
        $this->promoDiscountName = $promoDiscountName;
        return $this;
    }

    /**
     * Returns an array representation of this object.
     *
     * @return array
     */
    public function toArray(): array
    {
        $data = [];

        try {
            $reflector = new \ReflectionClass($this);
        } catch (\ReflectionException $e) {
            return $data;
        }

        foreach ($reflector->getProperties() as $property) {
            $property->setAccessible(true);
            $data[$property->getName()] = $property->getValue($this);
        }

        return $data;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function __toString()
    {
        $reflect = new \ReflectionClass($this);
        $str = '';

        foreach ($reflect->getProperties() as $prop) {
            $prop->setAccessible(true);
            $str .= $prop->getName() . ': ' . $prop->getValue($this) . ' ';
        }

        return $str;
    }

}
