<?php

namespace edu\wisc\services\cbs\order\line;

use edu\wisc\services\cbs\order\line\generated\InputParameters;

/**
 * Maps an {@link OrderLine} to an {@link InputParameters}.
 */
class OrderLineInputParametersMapper
{

    public static function toInputParameters(OrderLine $lineItem): InputParameters
    {
        return (new InputParameters(
            0.0,
            "",
            0.0,
            "",
            "",
            0.0,
            0.0,
            0.0,
            0.0,
            "",
            0.0,
            0.0,
            "",
            new \DateTime(),
            "",
            "",
            0.0,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            0.0,
            "",
            0.0,
            ""
        ))
            ->setP_ORDER_NUMBER($lineItem->getOrderNumber())
            ->setP_ORIG_SYS_DOCUMENT_REF($lineItem->getOrigSysDocumentRef())
            ->setP_LINE_TYPE($lineItem->getLineType())
            ->setP_LINE_NUMBER($lineItem->getLineNumber())
            ->setP_ORIG_SYS_LINE_REF($lineItem->getOrigSysLineRef())
            ->setP_ITEM_NUMBER_TYPE($lineItem->getItemNumberType())
            ->setP_ITEM_NUMBER($lineItem->getItemNumber())
            ->setP_QUANTITY($lineItem->getQuantity())
            ->setP_UNIT_PRICE($lineItem->getUnitPrice())
            ->setP_UNIT_PROMO_PRICE($lineItem->getUnitPromoPrice())
            ->setP_PROMO_DISCOUNT_NAME($lineItem->getPromoDiscountName())
            ->setP_BUNDLE_ID($lineItem->getBundleId())
            ->setP_BUNDLE_LINE_ID($lineItem->getBundleLineId())
            ->setP_DELIVERY_METHOD($lineItem->getDeliveryMethod())
            ->setP_SHIP_TO_IDENTIFIER($lineItem->getShipToIdentifier())
            ->setP_SHIP_TO_IDENTIFIER_TYPE($lineItem->getShipToIdentifierType())
            ->setP_SHIP_TO_ADDRESS_1($lineItem->getShipToAddress1())
            ->setP_SHIP_TO_ADDRESS_2($lineItem->getShipToAddress2())
            ->setP_SHIP_TO_ADDRESS_3($lineItem->getShipToAddress3())
            ->setP_SHIP_TO_ADDRESS_4($lineItem->getShipToAddress4())
            ->setP_SHIP_TO_CITY($lineItem->getShipToCity())
            ->setP_SHIP_TO_STATE($lineItem->getShipToState())
            ->setP_SHIP_TO_POSTAL_CODE($lineItem->getShipToPostalCode())
            ->setP_SHIPPING_INSTRUCTIONS($lineItem->getShippingInstructions())
            ->setP_SHIP_SET_ID($lineItem->getShipSetId())
            ->setP_USER_ID($lineItem->getUserId())
            ->setP_TEST_MODE($lineItem->getTestMode());
    }

}

