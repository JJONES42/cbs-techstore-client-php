<?php


 function autoload_7ac0539a110b123830eee3ec3d69c444($class)
{
    $classes = array(
        'edu\wisc\services\cbs\order\line\generated\DOIT_SOA_ORDER_IFACE_L_V3_Service' => __DIR__ .'/DOIT_SOA_ORDER_IFACE_L_V3_Service.php',
        'edu\wisc\services\cbs\order\line\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\order\line\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\order\line\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_7ac0539a110b123830eee3ec3d69c444');

// Do nothing. The rest is just leftovers from the code generation.
{
}
