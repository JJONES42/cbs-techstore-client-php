<?php

namespace edu\wisc\services\cbs\order\interfacing\generated;

class InputParameters
{

    /**
     * @var float $P_ORDER_NUMBER
     */
    protected $P_ORDER_NUMBER = null;

    /**
     * @var string $P_ORIG_SYS_DOCUMENT_REF
     */
    protected $P_ORIG_SYS_DOCUMENT_REF = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param float $P_ORDER_NUMBER
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ORDER_NUMBER, $P_ORIG_SYS_DOCUMENT_REF, $P_TEST_MODE)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return float
     */
    public function getP_ORDER_NUMBER()
    {
      return $this->P_ORDER_NUMBER;
    }

    /**
     * @param float $P_ORDER_NUMBER
     * @return \edu\wisc\services\cbs\order\interfacing\generated\InputParameters
     */
    public function setP_ORDER_NUMBER($P_ORDER_NUMBER)
    {
      $this->P_ORDER_NUMBER = $P_ORDER_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ORIG_SYS_DOCUMENT_REF()
    {
      return $this->P_ORIG_SYS_DOCUMENT_REF;
    }

    /**
     * @param string $P_ORIG_SYS_DOCUMENT_REF
     * @return \edu\wisc\services\cbs\order\interfacing\generated\InputParameters
     */
    public function setP_ORIG_SYS_DOCUMENT_REF($P_ORIG_SYS_DOCUMENT_REF)
    {
      $this->P_ORIG_SYS_DOCUMENT_REF = $P_ORIG_SYS_DOCUMENT_REF;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\order\interfacing\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
