<?php

namespace edu\wisc\services\cbs\order\interfacing\generated;

class DOIT_SOA_ORDER_IFACE_I_V2_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\order\\interfacing\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\order\\interfacing\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\order\\interfacing\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_order_iface_i_v2.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function INTERFACE_ORDER(InputParameters $body)
    {
      return $this->__soapCall('INTERFACE_ORDER', array($body));
    }

}
