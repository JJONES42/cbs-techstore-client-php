<?php

namespace edu\wisc\services\cbs\order\payment;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\order\payment\generated\DOIT_SOA_PAYMENT_V2_Service;
use edu\wisc\services\cbs\order\payment\generated\OutputParameters;

use Money\Money;

/**
 * A SOAP implementation of {@link OrderPaymentService}.
 */
class SoapOrderPaymentService implements OrderPaymentService, SoapService
{

    /** URL for QA WSDL */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_payment_v2/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_payment_v2/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_payment_v2/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_payment_v2/?wsdl';

    /** @var DOIT_SOA_PAYMENT_V2_Service */
    private $soapClient;

    /**
     * {@inheritdoc}
     */
    public function __construct($username, $password, $wsdlPath = null, \SoapClient $paymentSoapClient = null)
    {
        if ($paymentSoapClient !== null) {
            $this->soapClient = $paymentSoapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_PAYMENT_V2_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_PAYMENT_V2_Service(
                [],
                __DIR__ . '/../../../../../../resources/doit_soa_payment_v2.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createOrderPayment(OrderPayment $payment): OrderPaymentServiceResponse
    {
        /** @var OutputParameters */
        $outputParameters = $this->soapClient->PAYMENT_CREATE(
            OrderPaymentInputParametersMapper::toInputParameters($payment)
        );
        return new OrderPaymentServiceResponse(
            strcasecmp($outputParameters->getP_STATUS(), 'SUCCESS') === 0,
            $outputParameters->getP_RESULT_MESSAGE(),
            $outputParameters->getP_CHANGE_BACK(),
            $outputParameters->getP_BALANCE_DUE(),
            $outputParameters->getP_PAID_TO_DATE()
        );
    }

}
