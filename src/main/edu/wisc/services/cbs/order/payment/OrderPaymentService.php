<?php

namespace edu\wisc\services\cbs\order\payment;

use edu\wisc\services\cbs\api\Service;

interface OrderPaymentService extends Service
{

    /**
     * Submit an {@link OrderPayment} to CBS.
     *
     * @param OrderPayment $payment
     * @return OrderPaymentServiceResponse
     */
    public function createOrderPayment(OrderPayment $payment): OrderPaymentServiceResponse;

}