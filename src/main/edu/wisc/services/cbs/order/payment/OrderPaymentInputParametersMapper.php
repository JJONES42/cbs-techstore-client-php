<?php

namespace edu\wisc\services\cbs\order\payment;

use edu\wisc\services\cbs\order\payment\generated\InputParameters;

/**
 * Maps an {@link OrderPayment} to an {@link InputParameters} instance.
 */
class OrderPaymentInputParametersMapper
{

    /**
     * Maps an {@link OrderPayment} to an {@link InputParameters} instance.
     * @param OrderPayment $payment
     * @return InputParameters
     */
    public static function toInputParameters(OrderPayment $payment)
    {
        return (new InputParameters(
            0.0,
            '',
            '',
            '',
            '',
            0.0,
            0.0,
            0.0,
            0.0,
            '',
            '',
            '',
            new \DateTime(),
            ''
        ))
            ->setP_ORDER_NUMBER($payment->getOrderNumber())
            ->setP_ORIG_SYS_DOCUMENT_REF($payment->getOrigSysDocumentRef())
            ->setP_PAYMENT_INFO($payment->getPaymentInfo())
            ->setP_PAYMENT_TYPE($payment->getPaymentType())
            ->setP_CASH_REGISTER($payment->getRegister())
            ->setP_RENDERD_AMOUNT($payment->getRenderedAmount())
            ->setP_SUBTOTAL($payment->getSubtotal())
            ->setP_TAX($payment->getTax())
            ->setP_TOTAL($payment->getTotal())
            ->setP_PAYMENT_NOTES($payment->getPaymentNotes())
            ->setP_SALES_REP($payment->getSalesRep())
            ->setP_ORDER_SOURCE($payment->getOrderSource())
            ->setP_TEST_MODE($payment->getTestMode());
    }

}
