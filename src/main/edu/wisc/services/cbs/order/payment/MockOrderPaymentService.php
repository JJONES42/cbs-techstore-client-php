<?php

namespace edu\wisc\services\cbs\order\payment;

use edu\wisc\services\cbs\common\AbstractServiceResponse;

class MockOrderPaymentService implements OrderPaymentService
{

    /** @var AbstractServiceResponse */
    private $response;

    /**
     * {@inheritdoc}
     */
    public function __construct(OrderPaymentServiceResponse $response = null)
    {
        if ($response === null) {
            $this->response = new OrderPaymentServiceResponse(
                true,
                'Mock Order Payment Service Response',
                0.0,
                0.0,
                1.0
            );
        } else {
            $this->response = $response;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createOrderPayment(OrderPayment $payment): OrderPaymentServiceResponse
    {
        return $this->response;
    }

}