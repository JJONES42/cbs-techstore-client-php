<?php

namespace edu\wisc\services\cbs\order\payment;

use edu\wisc\services\cbs\common\AbstractServiceResponse;

/**
 * Response returned by {@link OrderPaymentService}.
 */
class OrderPaymentServiceResponse extends AbstractServiceResponse
{

    /** @var float */
    private $changeBack;

    /** @var float */
    private $balanceDue;

    /** @var float the amount that has been paid "to date" */
    private $paidToDate;

    /**
     * @param bool $success
     * @param string $message
     * @param float $changeBack
     * @param float $balanceDue
     * @param float $paidToDate
     */
    public function __construct(
        $success,
        $message,
        float $changeBack,
        float $balanceDue,
        float $paidToDate
    )
    {
        parent::__construct($success, $message);
        $this->changeBack = $changeBack;
        $this->balanceDue = $balanceDue;
        $this->paidToDate = $paidToDate;
    }

    /**
     * @return float
     */
    public function getChangeBack(): float
    {
        return $this->changeBack;
    }

    /**
     * @return float
     */
    public function getBalanceDue(): float
    {
        return $this->balanceDue;
    }

    /**
     * @return float
     */
    public function getPaidToDate(): float
    {
        return $this->paidToDate;
    }

}