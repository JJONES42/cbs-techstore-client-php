<?php

namespace edu\wisc\services\cbs\order\header;

use edu\wisc\services\cbs\api\Service;
use edu\wisc\services\cbs\order\OrderServiceResponse;

/**
 * Service intended for sending order "Headers" to CBS.
 */
interface OrderHeaderService extends Service
{

    /**
     * @param OrderHeader $header
     * @return OrderServiceResponse
     */
    public function createOrderHeader(OrderHeader $header): OrderServiceResponse;

}