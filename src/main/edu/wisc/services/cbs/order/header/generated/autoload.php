<?php


 function autoload_5e8551c1fe2631a0a06ae55c9a0d986a($class)
{
    $classes = array(
        'edu\wisc\services\cbs\order\header\generated\DOIT_SOA_ORDER_IFACE_H_V2_Service' => __DIR__ .'/DOIT_SOA_ORDER_IFACE_H_V2_Service.php',
        'edu\wisc\services\cbs\order\header\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\order\header\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\order\header\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_5e8551c1fe2631a0a06ae55c9a0d986a');

// Do nothing. The rest is just leftovers from the code generation.
{
}
