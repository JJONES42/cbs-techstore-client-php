<?php

namespace edu\wisc\services\cbs\order\header;

use edu\wisc\services\cbs\order\header\generated\InputParameters;

/**
 * Maps an {@link OrderHeader} to an {@link InputParameters}.
 */
class OrderHeaderInputParametersMapper
{

    /**
     * Maps an {@link OrderHeader} to an {@link InputParameters}.
     * @param OrderHeader $header
     * @return InputParameters
     */
    public static function toInputParameters(OrderHeader $header)
    {
        return (new InputParameters(
            0.0,
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ))
            ->setP_ORDER_NUMBER($header->getOrderNumber())
            ->setP_ORIG_SYS_DOCUMENT_REF($header->getOrigSystemDocumentRef())
            ->setP_CUSTOMER_TYPE($header->getCustomerType())
            ->setP_CUSTOMER_IDENTIFIER($header->getCustomerIdentifier())
            ->setP_CUSTOMER_IDENTIFIER_TYPE($header->getCustomerIdentifierType())
            ->setP_PURCHASER_IDENTIFIER($header->getPurchaserIdentifier())
            ->setP_PURCHASER_IDENTIFIER_TYPE($header->getPurchaserIdentifierType())
            ->setP_CUSTOMER_REFERENCE_FIELD($header->getCustomerReferenceField())
            ->setP_ORDER_SOURCE($header->getOrderSource())
            ->setP_INTERFACE_TYPE_FLAG($header->getInterfaceTypeFlag())
            ->setP_DEFAULT_SET($header->getDefaultSet())
            ->setP_TEST_MODE($header->getTestMode());
    }

}