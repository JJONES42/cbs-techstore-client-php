<?php

namespace edu\wisc\services\cbs\order\header;

use edu\wisc\services\cbs\api\MockService;
use edu\wisc\services\cbs\order\OrderServiceResponse;

class MockOrderHeaderService implements OrderHeaderService, MockService
{

    /** @var bool */
    private $success;

    /**
     * {@inheritdoc}
     */
    public function __construct(bool $success = true)
    {
        $this->success = $success;
    }

    /**
     * {@inheritdoc}
     */
    public function createOrderHeader(OrderHeader $header): OrderServiceResponse
    {
        return new OrderServiceResponse($this->success, (string)$header);
    }

}