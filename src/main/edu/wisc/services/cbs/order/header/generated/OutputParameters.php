<?php

namespace edu\wisc\services\cbs\order\header\generated;

class OutputParameters
{

    /**
     * @var string $P_STATUS
     */
    protected $P_STATUS = null;

    /**
     * @var string $P_RESULT_MESSAGE
     */
    protected $P_RESULT_MESSAGE = null;

    /**
     * @param string $P_STATUS
     * @param string $P_RESULT_MESSAGE
     */
    public function __construct($P_STATUS, $P_RESULT_MESSAGE)
    {
      $this->P_STATUS = $P_STATUS;
      $this->P_RESULT_MESSAGE = $P_RESULT_MESSAGE;
    }

    /**
     * @return string
     */
    public function getP_STATUS()
    {
      return $this->P_STATUS;
    }

    /**
     * @param string $P_STATUS
     * @return \edu\wisc\services\cbs\order\header\generated\OutputParameters
     */
    public function setP_STATUS($P_STATUS)
    {
      $this->P_STATUS = $P_STATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_RESULT_MESSAGE()
    {
      return $this->P_RESULT_MESSAGE;
    }

    /**
     * @param string $P_RESULT_MESSAGE
     * @return \edu\wisc\services\cbs\order\header\generated\OutputParameters
     */
    public function setP_RESULT_MESSAGE($P_RESULT_MESSAGE)
    {
      $this->P_RESULT_MESSAGE = $P_RESULT_MESSAGE;
      return $this;
    }

}
