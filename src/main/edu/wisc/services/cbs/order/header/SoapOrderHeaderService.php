<?php

namespace edu\wisc\services\cbs\order\header;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\order\header\generated\DOIT_SOA_ORDER_IFACE_H_V2_Service;
use edu\wisc\services\cbs\order\header\generated\OutputParameters;
use edu\wisc\services\cbs\order\OrderServiceResponse;

/**
 * SOAP implementation of {@link OrderHeaderService} for use with CBS.
 */
class SoapOrderHeaderService implements OrderHeaderService, SoapService
{

    /** URL for QA WSDL */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_order_iface_h_v2/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_order_iface_h_v2/?wsdl';
    /** URL for CP WSDL */
    const CBCP12= 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_order_iface_h_v2/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_order_iface_h_v2/?wsdl';

    /** @var DOIT_SOA_ORDER_IFACE_H_V2_Service */
    private $soapClient;

    /**
     * {@inheritdoc}
     */
    public function __construct($username, $password, $wsdlPath = null, \SoapClient $headerSoapClient = null)
    {
        if ($headerSoapClient !== null) {
            $this->soapClient = $headerSoapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_ORDER_IFACE_H_V2_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_ORDER_IFACE_H_V2_Service(
                [],
                __DIR__ . '/../../../../../../resources/doit_soa_order_iface_h_v2.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createOrderHeader(OrderHeader $header): OrderServiceResponse
    {
        /** @var OutputParameters */
        $outputParameters = $this->soapClient->HEADER_CREATE(
            OrderHeaderInputParametersMapper::toInputParameters($header)
        );
        return new OrderServiceResponse(
            strcasecmp($outputParameters->getP_STATUS(), 'SUCCESS') === 0,
            $outputParameters->getP_RESULT_MESSAGE()
        );
    }
}