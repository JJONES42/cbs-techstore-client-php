<?php
namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\exception\ProductServiceCreateException;
use edu\wisc\services\cbs\exception\ProductServiceUpdateException;
use edu\wisc\services\cbs\product\create\generated\DOIT_SOA_ITEM_CREATE_V7_Service;
use edu\wisc\services\cbs\product\create\generated\OutputParameters as CreateOutputParameters;
use edu\wisc\services\cbs\product\update\generated\DOIT_SOA_ITEM_UPDATE_V7_Service;
use edu\wisc\services\cbs\product\update\generated\OutputParameters as UpdateOutputParameters;

/**
 * Class SoapTechstoreClient uses SOAP to create a product in the CBS database.
 */
class SoapProductService implements ProductService
{

    /** URLS for QA WSDLs */
    const CBQA12_CREATE = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_item_create_v7/?wsdl';
    const CBQA12_UPDATE = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_item_update_v7/?wsdl';
    /** URLs for DV WSDLs */
    const CBDV12_CREATE = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_item_create_v7/?wsdl';
    const CBDV12_UPDATE = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_item_update_v7/?wsdl';
    /** URLS for CP WSDLS */
    const CBCP12_CREATE = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_item_create_v7/?wsdl';
    const CBCP12_UPDATE = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_item_update_v7/?wsdl';
    /** URLS for PROD WSDLs */
    const CBSP_CREATE = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_item_create_v7/?wsdl';
    const CBSP_UPDATE = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_item_update_v7/?wsdl';

    /** @var DOIT_SOA_ITEM_CREATE_V7_Service */
    private $productCreateSoapClient;

    /** @var DOIT_SOA_ITEM_UPDATE_V7_Service */
    private $productUpdateSoapClient;

    /**
     * A SOAP service is constructed with a username and password. If a {@link \SoapClient} is provided, the username
     * and password are ignored.
     *
     * @param string $username  username
     * @param string $password  password
     * @param string|null $createWsdlPath path to WSDL for specifying CBS environment. Defaults to QA
     * @param string|null $updateWsdlPath path to WSDL for specifying CBS environment. Defaults to QA
     * @param \SoapClient|null $productCreateSoapClient  Overriding SOAP client (for testing purposes)
     * @param \SoapClient|null $productUpdateSoapClient  Overriding SOAP client (for testing purposes)
     */
    public function __construct(
        $username,
        $password,
        $createWsdlPath = null,
        $updateWsdlPath = null,
        \SoapClient $productCreateSoapClient = null,
        \SoapClient $productUpdateSoapClient = null
    )
    {
        if ($createWsdlPath !== null && $updateWsdlPath !== null) {
            $this->productCreateSoapClient = new DOIT_SOA_ITEM_CREATE_V7_Service(
                [],
                $createWsdlPath
            );
            $this->productCreateSoapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken( $username, $password));

            $this->productUpdateSoapClient = new DOIT_SOA_ITEM_UPDATE_V7_Service(
                [],
                $updateWsdlPath
            );
            $this->productUpdateSoapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else if ($productCreateSoapClient === null || $productUpdateSoapClient === null) {
            $this->productCreateSoapClient = new DOIT_SOA_ITEM_CREATE_V7_Service(
                [],
                __DIR__ . '/../../../../../resources/doit_soa_item_create_v7.xml'
            );
            $this->productCreateSoapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken( $username, $password));

            $this->productUpdateSoapClient = new DOIT_SOA_ITEM_UPDATE_V7_Service(
                [],
                __DIR__ . '/../../../../../resources/doit_soa_item_update_v7.xml'
            );
            $this->productUpdateSoapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->productCreateSoapClient = $productCreateSoapClient;
            $this->productUpdateSoapClient = $productUpdateSoapClient;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function createProduct(Product $product): ProductServiceResponse
    {
        try {
            $outputParameters = $this->productCreateSoapClient->ITEM_CREATE(
                ProductInputParametersMapper::toInputParameters(
                    $product,
                    ProductInputParametersMapper::$CREATE
                )
            );
            return static::createResponse($outputParameters);
        } catch (\Exception $e) {
            throw new ProductServiceCreateException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function updateProduct(Product $product): ProductServiceResponse
    {
        try {
            $outputParameters = $this->productUpdateSoapClient->ITEM_UPDATE(
                ProductInputParametersMapper::toInputParameters(
                    $product,
                    ProductInputParametersMapper::$UPDATE
                )
            );
            return static::createResponse($outputParameters);
        } catch (\Exception $e) {
            throw new ProductServiceUpdateException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param CreateOutputParameters|UpdateOutputParameters $outputParameters
     * @return ProductServiceResponse
     */
    public static function createResponse($outputParameters)
    {
        return new ProductServiceResponse(
            strcasecmp($outputParameters->getP_STATUS(), 'SUCCESS') == 0,
            $outputParameters->getP_RESULT_MESSAGE()
        );
    }
}
