<?php

namespace edu\wisc\services\cbs\product\create\generated;

class InputParameters
{

    /**
     * @var string $P_ITEM_NUMBER
     */
    protected $P_ITEM_NUMBER = null;

    /**
     * @var string $P_ITEM_DESCRIPTION
     */
    protected $P_ITEM_DESCRIPTION = null;

    /**
     * @var string $P_CBS_ITEM_TEMPLATE
     */
    protected $P_CBS_ITEM_TEMPLATE = null;

    /**
     * @var string $P_ITEM_CATEGORY
     */
    protected $P_ITEM_CATEGORY = null;

    /**
     * @var string $P_TAX_CLASS_ID
     */
    protected $P_TAX_CLASS_ID = null;

    /**
     * @var string $P_MANUFACTURER_NAME
     */
    protected $P_MANUFACTURER_NAME = null;

    /**
     * @var string $P_MANUFACTURER_PART_NUMBER
     */
    protected $P_MANUFACTURER_PART_NUMBER = null;

    /**
     * @var float $P_COST
     */
    protected $P_COST = null;

    /**
     * @var float $P_MINMAX_LEVEL_MIN
     */
    protected $P_MINMAX_LEVEL_MIN = null;

    /**
     * @var float $P_MINMAX_LEVEL_MAX
     */
    protected $P_MINMAX_LEVEL_MAX = null;

    /**
     * @var string $P_LIFE_CYCLE
     */
    protected $P_LIFE_CYCLE = null;

    /**
     * @var string $P_SERIAL_CONTROL_FLAG
     */
    protected $P_SERIAL_CONTROL_FLAG = null;

    /**
     * @var string $P_SERIAL_START_STRING
     */
    protected $P_SERIAL_START_STRING = null;

    /**
     * @var string $P_SERIAL_END_STRING
     */
    protected $P_SERIAL_END_STRING = null;

    /**
     * @var float $P_SERIAL_LENGTH_MIN
     */
    protected $P_SERIAL_LENGTH_MIN = null;

    /**
     * @var float $P_SERIAL_LENGTH_MAX
     */
    protected $P_SERIAL_LENGTH_MAX = null;

    /**
     * @var string $P_VENDOR
     */
    protected $P_VENDOR = null;

    /**
     * @var string $P_SUPPLIER_PART_NUMBER
     */
    protected $P_SUPPLIER_PART_NUMBER = null;

    /**
     * @var string $P_UPC
     */
    protected $P_UPC = null;

    /**
     * @var string $P_UPC_ALTERNATE
     */
    protected $P_UPC_ALTERNATE = null;

    /**
     * @var float $P_BASE_PRICE
     */
    protected $P_BASE_PRICE = null;

    /**
     * @var string $P_PRICING_TEMPLATE
     */
    protected $P_PRICING_TEMPLATE = null;

    /**
     * @var string $P_REFERENCE
     */
    protected $P_REFERENCE = null;

    /**
     * @var float $P_COST_CENTER
     */
    protected $P_COST_CENTER = null;

    /**
     * @var string $P_PLANNER_CODE
     */
    protected $P_PLANNER_CODE = null;

    /**
     * @var string $P_MINMAX_ACTIVE
     */
    protected $P_MINMAX_ACTIVE = null;

    /**
     * @var string $P_BUYER
     */
    protected $P_BUYER = null;

    /**
     * @var string $P_PRODUCT_MANAGER
     */
    protected $P_PRODUCT_MANAGER = null;

    /**
     * @var string $P_INVENTORY_CONTROL_TYPE
     */
    protected $P_INVENTORY_CONTROL_TYPE = null;

    /**
     * @var float $P_FIXED_LOT_MULTIPLIER
     */
    protected $P_FIXED_LOT_MULTIPLIER = null;

    /**
     * @var string $P_SALES_POINT
     */
    protected $P_SALES_POINT = null;

    /**
     * @var string $P_PS_PRIMARY_LOCATOR
     */
    protected $P_PS_PRIMARY_LOCATOR = null;

    /**
     * @var string $P_PS_SECONDARY_LOCATOR
     */
    protected $P_PS_SECONDARY_LOCATOR = null;

    /**
     * @var float $P_PS_MIN
     */
    protected $P_PS_MIN = null;

    /**
     * @var float $P_PS_MAX
     */
    protected $P_PS_MAX = null;

    /**
     * @var string $P_HSLC_PRIMARY_LOCATOR
     */
    protected $P_HSLC_PRIMARY_LOCATOR = null;

    /**
     * @var string $P_HSLC_SECONDARY_LOCATOR
     */
    protected $P_HSLC_SECONDARY_LOCATOR = null;

    /**
     * @var float $P_HSLC_MIN
     */
    protected $P_HSLC_MIN = null;

    /**
     * @var float $P_HSLC_MAX
     */
    protected $P_HSLC_MAX = null;

    /**
     * @var string $P_ECM_PRIMARY_LOCATOR
     */
    protected $P_ECM_PRIMARY_LOCATOR = null;

    /**
     * @var string $P_ECM_SECONDARY_LOCATOR
     */
    protected $P_ECM_SECONDARY_LOCATOR = null;

    /**
     * @var float $P_ECM_MIN
     */
    protected $P_ECM_MIN = null;

    /**
     * @var float $P_ECM_MAX
     */
    protected $P_ECM_MAX = null;

    /**
     * @var string $P_DEP_ELIGIBLE
     */
    protected $P_DEP_ELIGIBLE = null;

    /**
     * @var string $P_ADD_SALES_INSTRUCTIONS
     */
    protected $P_ADD_SALES_INSTRUCTIONS = null;

    /**
     * @var string $P_SALES_PROCEDURE
     */
    protected $P_SALES_PROCEDURE = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param string $P_ITEM_NUMBER
     * @param string $P_ITEM_DESCRIPTION
     * @param string $P_CBS_ITEM_TEMPLATE
     * @param string $P_ITEM_CATEGORY
     * @param string $P_TAX_CLASS_ID
     * @param string $P_MANUFACTURER_NAME
     * @param string $P_MANUFACTURER_PART_NUMBER
     * @param float $P_COST
     * @param float $P_MINMAX_LEVEL_MIN
     * @param float $P_MINMAX_LEVEL_MAX
     * @param string $P_LIFE_CYCLE
     * @param string $P_SERIAL_CONTROL_FLAG
     * @param string $P_SERIAL_START_STRING
     * @param string $P_SERIAL_END_STRING
     * @param float $P_SERIAL_LENGTH_MIN
     * @param float $P_SERIAL_LENGTH_MAX
     * @param string $P_VENDOR
     * @param string $P_SUPPLIER_PART_NUMBER
     * @param string $P_UPC
     * @param string $P_UPC_ALTERNATE
     * @param float $P_BASE_PRICE
     * @param string $P_PRICING_TEMPLATE
     * @param string $P_REFERENCE
     * @param float $P_COST_CENTER
     * @param string $P_PLANNER_CODE
     * @param string $P_MINMAX_ACTIVE
     * @param string $P_BUYER
     * @param string $P_PRODUCT_MANAGER
     * @param string $P_INVENTORY_CONTROL_TYPE
     * @param float $P_FIXED_LOT_MULTIPLIER
     * @param string $P_SALES_POINT
     * @param string $P_PS_PRIMARY_LOCATOR
     * @param string $P_PS_SECONDARY_LOCATOR
     * @param float $P_PS_MIN
     * @param float $P_PS_MAX
     * @param string $P_HSLC_PRIMARY_LOCATOR
     * @param string $P_HSLC_SECONDARY_LOCATOR
     * @param float $P_HSLC_MIN
     * @param float $P_HSLC_MAX
     * @param string $P_ECM_PRIMARY_LOCATOR
     * @param string $P_ECM_SECONDARY_LOCATOR
     * @param float $P_ECM_MIN
     * @param float $P_ECM_MAX
     * @param string $P_DEP_ELIGIBLE
     * @param string $P_ADD_SALES_INSTRUCTIONS
     * @param string $P_SALES_PROCEDURE
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ITEM_NUMBER, $P_ITEM_DESCRIPTION, $P_CBS_ITEM_TEMPLATE, $P_ITEM_CATEGORY, $P_TAX_CLASS_ID, $P_MANUFACTURER_NAME, $P_MANUFACTURER_PART_NUMBER, $P_COST, $P_MINMAX_LEVEL_MIN, $P_MINMAX_LEVEL_MAX, $P_LIFE_CYCLE, $P_SERIAL_CONTROL_FLAG, $P_SERIAL_START_STRING, $P_SERIAL_END_STRING, $P_SERIAL_LENGTH_MIN, $P_SERIAL_LENGTH_MAX, $P_VENDOR, $P_SUPPLIER_PART_NUMBER, $P_UPC, $P_UPC_ALTERNATE, $P_BASE_PRICE, $P_PRICING_TEMPLATE, $P_REFERENCE, $P_COST_CENTER, $P_PLANNER_CODE, $P_MINMAX_ACTIVE, $P_BUYER, $P_PRODUCT_MANAGER, $P_INVENTORY_CONTROL_TYPE, $P_FIXED_LOT_MULTIPLIER, $P_SALES_POINT, $P_PS_PRIMARY_LOCATOR, $P_PS_SECONDARY_LOCATOR, $P_PS_MIN, $P_PS_MAX, $P_HSLC_PRIMARY_LOCATOR, $P_HSLC_SECONDARY_LOCATOR, $P_HSLC_MIN, $P_HSLC_MAX, $P_ECM_PRIMARY_LOCATOR, $P_ECM_SECONDARY_LOCATOR, $P_ECM_MIN, $P_ECM_MAX, $P_DEP_ELIGIBLE, $P_ADD_SALES_INSTRUCTIONS, $P_SALES_PROCEDURE, $P_TEST_MODE)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      $this->P_ITEM_DESCRIPTION = $P_ITEM_DESCRIPTION;
      $this->P_CBS_ITEM_TEMPLATE = $P_CBS_ITEM_TEMPLATE;
      $this->P_ITEM_CATEGORY = $P_ITEM_CATEGORY;
      $this->P_TAX_CLASS_ID = $P_TAX_CLASS_ID;
      $this->P_MANUFACTURER_NAME = $P_MANUFACTURER_NAME;
      $this->P_MANUFACTURER_PART_NUMBER = $P_MANUFACTURER_PART_NUMBER;
      $this->P_COST = $P_COST;
      $this->P_MINMAX_LEVEL_MIN = $P_MINMAX_LEVEL_MIN;
      $this->P_MINMAX_LEVEL_MAX = $P_MINMAX_LEVEL_MAX;
      $this->P_LIFE_CYCLE = $P_LIFE_CYCLE;
      $this->P_SERIAL_CONTROL_FLAG = $P_SERIAL_CONTROL_FLAG;
      $this->P_SERIAL_START_STRING = $P_SERIAL_START_STRING;
      $this->P_SERIAL_END_STRING = $P_SERIAL_END_STRING;
      $this->P_SERIAL_LENGTH_MIN = $P_SERIAL_LENGTH_MIN;
      $this->P_SERIAL_LENGTH_MAX = $P_SERIAL_LENGTH_MAX;
      $this->P_VENDOR = $P_VENDOR;
      $this->P_SUPPLIER_PART_NUMBER = $P_SUPPLIER_PART_NUMBER;
      $this->P_UPC = $P_UPC;
      $this->P_UPC_ALTERNATE = $P_UPC_ALTERNATE;
      $this->P_BASE_PRICE = $P_BASE_PRICE;
      $this->P_PRICING_TEMPLATE = $P_PRICING_TEMPLATE;
      $this->P_REFERENCE = $P_REFERENCE;
      $this->P_COST_CENTER = $P_COST_CENTER;
      $this->P_PLANNER_CODE = $P_PLANNER_CODE;
      $this->P_MINMAX_ACTIVE = $P_MINMAX_ACTIVE;
      $this->P_BUYER = $P_BUYER;
      $this->P_PRODUCT_MANAGER = $P_PRODUCT_MANAGER;
      $this->P_INVENTORY_CONTROL_TYPE = $P_INVENTORY_CONTROL_TYPE;
      $this->P_FIXED_LOT_MULTIPLIER = $P_FIXED_LOT_MULTIPLIER;
      $this->P_SALES_POINT = $P_SALES_POINT;
      $this->P_PS_PRIMARY_LOCATOR = $P_PS_PRIMARY_LOCATOR;
      $this->P_PS_SECONDARY_LOCATOR = $P_PS_SECONDARY_LOCATOR;
      $this->P_PS_MIN = $P_PS_MIN;
      $this->P_PS_MAX = $P_PS_MAX;
      $this->P_HSLC_PRIMARY_LOCATOR = $P_HSLC_PRIMARY_LOCATOR;
      $this->P_HSLC_SECONDARY_LOCATOR = $P_HSLC_SECONDARY_LOCATOR;
      $this->P_HSLC_MIN = $P_HSLC_MIN;
      $this->P_HSLC_MAX = $P_HSLC_MAX;
      $this->P_ECM_PRIMARY_LOCATOR = $P_ECM_PRIMARY_LOCATOR;
      $this->P_ECM_SECONDARY_LOCATOR = $P_ECM_SECONDARY_LOCATOR;
      $this->P_ECM_MIN = $P_ECM_MIN;
      $this->P_ECM_MAX = $P_ECM_MAX;
      $this->P_DEP_ELIGIBLE = $P_DEP_ELIGIBLE;
      $this->P_ADD_SALES_INSTRUCTIONS = $P_ADD_SALES_INSTRUCTIONS;
      $this->P_SALES_PROCEDURE = $P_SALES_PROCEDURE;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return string
     */
    public function getP_ITEM_NUMBER()
    {
      return $this->P_ITEM_NUMBER;
    }

    /**
     * @param string $P_ITEM_NUMBER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ITEM_NUMBER($P_ITEM_NUMBER)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ITEM_DESCRIPTION()
    {
      return $this->P_ITEM_DESCRIPTION;
    }

    /**
     * @param string $P_ITEM_DESCRIPTION
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ITEM_DESCRIPTION($P_ITEM_DESCRIPTION)
    {
      $this->P_ITEM_DESCRIPTION = $P_ITEM_DESCRIPTION;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_CBS_ITEM_TEMPLATE()
    {
      return $this->P_CBS_ITEM_TEMPLATE;
    }

    /**
     * @param string $P_CBS_ITEM_TEMPLATE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_CBS_ITEM_TEMPLATE($P_CBS_ITEM_TEMPLATE)
    {
      $this->P_CBS_ITEM_TEMPLATE = $P_CBS_ITEM_TEMPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ITEM_CATEGORY()
    {
      return $this->P_ITEM_CATEGORY;
    }

    /**
     * @param string $P_ITEM_CATEGORY
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ITEM_CATEGORY($P_ITEM_CATEGORY)
    {
      $this->P_ITEM_CATEGORY = $P_ITEM_CATEGORY;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TAX_CLASS_ID()
    {
      return $this->P_TAX_CLASS_ID;
    }

    /**
     * @param string $P_TAX_CLASS_ID
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_TAX_CLASS_ID($P_TAX_CLASS_ID)
    {
      $this->P_TAX_CLASS_ID = $P_TAX_CLASS_ID;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_MANUFACTURER_NAME()
    {
      return $this->P_MANUFACTURER_NAME;
    }

    /**
     * @param string $P_MANUFACTURER_NAME
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_MANUFACTURER_NAME($P_MANUFACTURER_NAME)
    {
      $this->P_MANUFACTURER_NAME = $P_MANUFACTURER_NAME;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_MANUFACTURER_PART_NUMBER()
    {
      return $this->P_MANUFACTURER_PART_NUMBER;
    }

    /**
     * @param string $P_MANUFACTURER_PART_NUMBER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_MANUFACTURER_PART_NUMBER($P_MANUFACTURER_PART_NUMBER)
    {
      $this->P_MANUFACTURER_PART_NUMBER = $P_MANUFACTURER_PART_NUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_COST()
    {
      return $this->P_COST;
    }

    /**
     * @param float $P_COST
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_COST($P_COST)
    {
      $this->P_COST = $P_COST;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_MINMAX_LEVEL_MIN()
    {
      return $this->P_MINMAX_LEVEL_MIN;
    }

    /**
     * @param float $P_MINMAX_LEVEL_MIN
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_MINMAX_LEVEL_MIN($P_MINMAX_LEVEL_MIN)
    {
      $this->P_MINMAX_LEVEL_MIN = $P_MINMAX_LEVEL_MIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_MINMAX_LEVEL_MAX()
    {
      return $this->P_MINMAX_LEVEL_MAX;
    }

    /**
     * @param float $P_MINMAX_LEVEL_MAX
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_MINMAX_LEVEL_MAX($P_MINMAX_LEVEL_MAX)
    {
      $this->P_MINMAX_LEVEL_MAX = $P_MINMAX_LEVEL_MAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_LIFE_CYCLE()
    {
      return $this->P_LIFE_CYCLE;
    }

    /**
     * @param string $P_LIFE_CYCLE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_LIFE_CYCLE($P_LIFE_CYCLE)
    {
      $this->P_LIFE_CYCLE = $P_LIFE_CYCLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SERIAL_CONTROL_FLAG()
    {
      return $this->P_SERIAL_CONTROL_FLAG;
    }

    /**
     * @param string $P_SERIAL_CONTROL_FLAG
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SERIAL_CONTROL_FLAG($P_SERIAL_CONTROL_FLAG)
    {
      $this->P_SERIAL_CONTROL_FLAG = $P_SERIAL_CONTROL_FLAG;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SERIAL_START_STRING()
    {
      return $this->P_SERIAL_START_STRING;
    }

    /**
     * @param string $P_SERIAL_START_STRING
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SERIAL_START_STRING($P_SERIAL_START_STRING)
    {
      $this->P_SERIAL_START_STRING = $P_SERIAL_START_STRING;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SERIAL_END_STRING()
    {
      return $this->P_SERIAL_END_STRING;
    }

    /**
     * @param string $P_SERIAL_END_STRING
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SERIAL_END_STRING($P_SERIAL_END_STRING)
    {
      $this->P_SERIAL_END_STRING = $P_SERIAL_END_STRING;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_SERIAL_LENGTH_MIN()
    {
      return $this->P_SERIAL_LENGTH_MIN;
    }

    /**
     * @param float $P_SERIAL_LENGTH_MIN
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SERIAL_LENGTH_MIN($P_SERIAL_LENGTH_MIN)
    {
      $this->P_SERIAL_LENGTH_MIN = $P_SERIAL_LENGTH_MIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_SERIAL_LENGTH_MAX()
    {
      return $this->P_SERIAL_LENGTH_MAX;
    }

    /**
     * @param float $P_SERIAL_LENGTH_MAX
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SERIAL_LENGTH_MAX($P_SERIAL_LENGTH_MAX)
    {
      $this->P_SERIAL_LENGTH_MAX = $P_SERIAL_LENGTH_MAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_VENDOR()
    {
      return $this->P_VENDOR;
    }

    /**
     * @param string $P_VENDOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_VENDOR($P_VENDOR)
    {
      $this->P_VENDOR = $P_VENDOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SUPPLIER_PART_NUMBER()
    {
      return $this->P_SUPPLIER_PART_NUMBER;
    }

    /**
     * @param string $P_SUPPLIER_PART_NUMBER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SUPPLIER_PART_NUMBER($P_SUPPLIER_PART_NUMBER)
    {
      $this->P_SUPPLIER_PART_NUMBER = $P_SUPPLIER_PART_NUMBER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_UPC()
    {
      return $this->P_UPC;
    }

    /**
     * @param string $P_UPC
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_UPC($P_UPC)
    {
      $this->P_UPC = $P_UPC;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_UPC_ALTERNATE()
    {
      return $this->P_UPC_ALTERNATE;
    }

    /**
     * @param string $P_UPC_ALTERNATE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_UPC_ALTERNATE($P_UPC_ALTERNATE)
    {
      $this->P_UPC_ALTERNATE = $P_UPC_ALTERNATE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_BASE_PRICE()
    {
      return $this->P_BASE_PRICE;
    }

    /**
     * @param float $P_BASE_PRICE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_BASE_PRICE($P_BASE_PRICE)
    {
      $this->P_BASE_PRICE = $P_BASE_PRICE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PRICING_TEMPLATE()
    {
      return $this->P_PRICING_TEMPLATE;
    }

    /**
     * @param string $P_PRICING_TEMPLATE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PRICING_TEMPLATE($P_PRICING_TEMPLATE)
    {
      $this->P_PRICING_TEMPLATE = $P_PRICING_TEMPLATE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_REFERENCE()
    {
      return $this->P_REFERENCE;
    }

    /**
     * @param string $P_REFERENCE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_REFERENCE($P_REFERENCE)
    {
      $this->P_REFERENCE = $P_REFERENCE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_COST_CENTER()
    {
      return $this->P_COST_CENTER;
    }

    /**
     * @param float $P_COST_CENTER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_COST_CENTER($P_COST_CENTER)
    {
      $this->P_COST_CENTER = $P_COST_CENTER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PLANNER_CODE()
    {
      return $this->P_PLANNER_CODE;
    }

    /**
     * @param string $P_PLANNER_CODE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PLANNER_CODE($P_PLANNER_CODE)
    {
      $this->P_PLANNER_CODE = $P_PLANNER_CODE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_MINMAX_ACTIVE()
    {
      return $this->P_MINMAX_ACTIVE;
    }

    /**
     * @param string $P_MINMAX_ACTIVE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_MINMAX_ACTIVE($P_MINMAX_ACTIVE)
    {
      $this->P_MINMAX_ACTIVE = $P_MINMAX_ACTIVE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_BUYER()
    {
      return $this->P_BUYER;
    }

    /**
     * @param string $P_BUYER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_BUYER($P_BUYER)
    {
      $this->P_BUYER = $P_BUYER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PRODUCT_MANAGER()
    {
      return $this->P_PRODUCT_MANAGER;
    }

    /**
     * @param string $P_PRODUCT_MANAGER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PRODUCT_MANAGER($P_PRODUCT_MANAGER)
    {
      $this->P_PRODUCT_MANAGER = $P_PRODUCT_MANAGER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_INVENTORY_CONTROL_TYPE()
    {
      return $this->P_INVENTORY_CONTROL_TYPE;
    }

    /**
     * @param string $P_INVENTORY_CONTROL_TYPE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_INVENTORY_CONTROL_TYPE($P_INVENTORY_CONTROL_TYPE)
    {
      $this->P_INVENTORY_CONTROL_TYPE = $P_INVENTORY_CONTROL_TYPE;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_FIXED_LOT_MULTIPLIER()
    {
      return $this->P_FIXED_LOT_MULTIPLIER;
    }

    /**
     * @param float $P_FIXED_LOT_MULTIPLIER
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_FIXED_LOT_MULTIPLIER($P_FIXED_LOT_MULTIPLIER)
    {
      $this->P_FIXED_LOT_MULTIPLIER = $P_FIXED_LOT_MULTIPLIER;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SALES_POINT()
    {
      return $this->P_SALES_POINT;
    }

    /**
     * @param string $P_SALES_POINT
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SALES_POINT($P_SALES_POINT)
    {
      $this->P_SALES_POINT = $P_SALES_POINT;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PS_PRIMARY_LOCATOR()
    {
      return $this->P_PS_PRIMARY_LOCATOR;
    }

    /**
     * @param string $P_PS_PRIMARY_LOCATOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PS_PRIMARY_LOCATOR($P_PS_PRIMARY_LOCATOR)
    {
      $this->P_PS_PRIMARY_LOCATOR = $P_PS_PRIMARY_LOCATOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_PS_SECONDARY_LOCATOR()
    {
      return $this->P_PS_SECONDARY_LOCATOR;
    }

    /**
     * @param string $P_PS_SECONDARY_LOCATOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PS_SECONDARY_LOCATOR($P_PS_SECONDARY_LOCATOR)
    {
      $this->P_PS_SECONDARY_LOCATOR = $P_PS_SECONDARY_LOCATOR;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_PS_MIN()
    {
      return $this->P_PS_MIN;
    }

    /**
     * @param float $P_PS_MIN
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PS_MIN($P_PS_MIN)
    {
      $this->P_PS_MIN = $P_PS_MIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_PS_MAX()
    {
      return $this->P_PS_MAX;
    }

    /**
     * @param float $P_PS_MAX
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_PS_MAX($P_PS_MAX)
    {
      $this->P_PS_MAX = $P_PS_MAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_HSLC_PRIMARY_LOCATOR()
    {
      return $this->P_HSLC_PRIMARY_LOCATOR;
    }

    /**
     * @param string $P_HSLC_PRIMARY_LOCATOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_HSLC_PRIMARY_LOCATOR($P_HSLC_PRIMARY_LOCATOR)
    {
      $this->P_HSLC_PRIMARY_LOCATOR = $P_HSLC_PRIMARY_LOCATOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_HSLC_SECONDARY_LOCATOR()
    {
      return $this->P_HSLC_SECONDARY_LOCATOR;
    }

    /**
     * @param string $P_HSLC_SECONDARY_LOCATOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_HSLC_SECONDARY_LOCATOR($P_HSLC_SECONDARY_LOCATOR)
    {
      $this->P_HSLC_SECONDARY_LOCATOR = $P_HSLC_SECONDARY_LOCATOR;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_HSLC_MIN()
    {
      return $this->P_HSLC_MIN;
    }

    /**
     * @param float $P_HSLC_MIN
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_HSLC_MIN($P_HSLC_MIN)
    {
      $this->P_HSLC_MIN = $P_HSLC_MIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_HSLC_MAX()
    {
      return $this->P_HSLC_MAX;
    }

    /**
     * @param float $P_HSLC_MAX
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_HSLC_MAX($P_HSLC_MAX)
    {
      $this->P_HSLC_MAX = $P_HSLC_MAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ECM_PRIMARY_LOCATOR()
    {
      return $this->P_ECM_PRIMARY_LOCATOR;
    }

    /**
     * @param string $P_ECM_PRIMARY_LOCATOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ECM_PRIMARY_LOCATOR($P_ECM_PRIMARY_LOCATOR)
    {
      $this->P_ECM_PRIMARY_LOCATOR = $P_ECM_PRIMARY_LOCATOR;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ECM_SECONDARY_LOCATOR()
    {
      return $this->P_ECM_SECONDARY_LOCATOR;
    }

    /**
     * @param string $P_ECM_SECONDARY_LOCATOR
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ECM_SECONDARY_LOCATOR($P_ECM_SECONDARY_LOCATOR)
    {
      $this->P_ECM_SECONDARY_LOCATOR = $P_ECM_SECONDARY_LOCATOR;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_ECM_MIN()
    {
      return $this->P_ECM_MIN;
    }

    /**
     * @param float $P_ECM_MIN
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ECM_MIN($P_ECM_MIN)
    {
      $this->P_ECM_MIN = $P_ECM_MIN;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_ECM_MAX()
    {
      return $this->P_ECM_MAX;
    }

    /**
     * @param float $P_ECM_MAX
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ECM_MAX($P_ECM_MAX)
    {
      $this->P_ECM_MAX = $P_ECM_MAX;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_DEP_ELIGIBLE()
    {
      return $this->P_DEP_ELIGIBLE;
    }

    /**
     * @param string $P_DEP_ELIGIBLE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_DEP_ELIGIBLE($P_DEP_ELIGIBLE)
    {
      $this->P_DEP_ELIGIBLE = $P_DEP_ELIGIBLE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ADD_SALES_INSTRUCTIONS()
    {
      return $this->P_ADD_SALES_INSTRUCTIONS;
    }

    /**
     * @param string $P_ADD_SALES_INSTRUCTIONS
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_ADD_SALES_INSTRUCTIONS($P_ADD_SALES_INSTRUCTIONS)
    {
      $this->P_ADD_SALES_INSTRUCTIONS = $P_ADD_SALES_INSTRUCTIONS;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_SALES_PROCEDURE()
    {
      return $this->P_SALES_PROCEDURE;
    }

    /**
     * @param string $P_SALES_PROCEDURE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_SALES_PROCEDURE($P_SALES_PROCEDURE)
    {
      $this->P_SALES_PROCEDURE = $P_SALES_PROCEDURE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\product\create\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
