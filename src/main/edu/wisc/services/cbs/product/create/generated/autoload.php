<?php


 function autoload_b29e0ff5ae9b45b654cc62742f24565d($class)
{
    $classes = array(
        'edu\wisc\services\cbs\product\create\generated\DOIT_SOA_ITEM_CREATE_V7_Service' => __DIR__ .'/DOIT_SOA_ITEM_CREATE_V7_Service.php',
        'edu\wisc\services\cbs\product\create\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\product\create\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\product\create\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_b29e0ff5ae9b45b654cc62742f24565d');

// Do nothing. The rest is just leftovers from the code generation.
{
}
