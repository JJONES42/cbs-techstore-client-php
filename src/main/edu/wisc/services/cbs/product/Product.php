<?php

namespace edu\wisc\services\cbs\product;

/**
 * Intermediate representation of a "Product", independent of system (e.g. Magento, CBS).
 *
 * @author Nicholas Blair
 */
class Product
{

    /** @var string CBS value for MinMax active */
    const MIN_MAX_ACTIVE = 'yes';

    /** @var string CBS value for MinMax inactive */
    const MIN_MAX_INACTIVE = 'no';

    /** @var string  Stock Keeping Unit, a unique product identifier */
    private $itemNumber;

    /** @var string  name / short description */
    private $description = '';

    /** @var string  CBS Item Template */
    private $cbsItemTemplate;

    /** @var float  cost */
    private $cost;

    /** @var string */
    private $itemCategory = '';

    /** @var string */
    private $taxClassId;

    /** @var string */
    private $manufacturer = '';

    /** @var string */
    private $manufacturerPartNumber = '';

    /** @var float  minimum quantity allowed in inventory */
    private $minQuantity;

    /** @var float  maximum quantity allowed in inventory */
    private $maxQuantity;

    /** @var string */
    private $pricingTemplate;

    /** @var string  state of the item within the Techstore, e.g. 'FULLY_SELLABLE' or 'FROZEN'*/
    private $lifecycle = '';

    /** @var string  control flag for capturing serial numbers */
    private $serialControlFlag = '';

    /** @var string */
    private $serialStartString;

    /** @var string */
    private $serialEndString;

    /** @var float */
    private $serialLengthMax;

    /** @var float */
    private $serialLengthMin;

    /** @var string  product vendor */
    private $vendor = '';

    /** @var string  vendor part number */
    private $vendorPartNumber = '';

    /** @var string  UPC (barcode) */
    private $upc = '';

    /** @var string  UPC alternate (barcode) */
    private $upcAlternate = '';

    /** @var float base price */
    private $basePrice;

    /** @var string */
    private $reference;

    /** @var string cost center (aka UDDS) */
    private $costCenter;

    /** @var string planner code */
    private $plannerCode;

    /** @var string min/max active */
    private $minMaxActive;

    /** @var string buyer */
    private $buyer;

    /** @var string product manager */
    private $productManager;

    /** @var string */
    private $inventoryControlType;

    /** @var float */
    private $fixedLotMultiplier;

    /** @var string */
    private $salesPoint;

    /** @var string */
    private $psPrimaryLocator;

    /** @var string */
    private $psSecondaryLocator;

    /** @var float */
    private $psMin;

    /** @var float */
    private $psMax;

    /** @var string */
    private $hslcPrimaryLocator;

    /** @var string */
    private $hslcSecondaryLocator;

    /** @var float */
    private $hslcMin;

    /** @var float */
    private $hslcMax;

    /** @var string */
    private $ecmPrimaryLocator;

    /** @var string */
    private $ecmSecondaryLocator;

    /** @var float */
    private $ecmMin;

    /** @var float */
    private $ecmMax;

    /** @var string */
    private $depEligible;

    /** @var string */
    private $addSalesInstructions;

    /** @var string */
    private $salesProcedure;

    /** @var string */
    private $testMode;

    /**
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }
    /**
     * @param string $itemNumber
     * @return Product
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     * @return Product
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemCategory()
    {
        return $this->itemCategory;
    }

    /**
     * @param string $itemCategory
     * @return Product
     */
    public function setItemCategory($itemCategory)
    {
        $this->itemCategory = $itemCategory;
        return $this;
    }

    /**
     * @return string
     */
    public function getTaxClassId()
    {
        return $this->taxClassId;
    }

    /**
     * @param string|null $taxClassId
     * @return Product
     */
    public function setTaxClassId($taxClassId)
    {
        $this->taxClassId = $taxClassId;
        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param string $manufacturer
     * @return Product
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getManufacturerPartNumber()
    {
        return $this->manufacturerPartNumber;
    }

    /**
     * @param string|null $manufacturerPartNumber
     * @return Product
     */
    public function setManufacturerPartNumber($manufacturerPartNumber)
    {
        $this->manufacturerPartNumber = $manufacturerPartNumber;
        return $this;
    }

    /**
     * @return float
     */
    public function getMinQuantity()
    {
        return $this->minQuantity;
    }

    /**
     * @param float $minQuantity
     * @return Product
     */
    public function setMinQuantity($minQuantity)
    {
        $this->minQuantity = $minQuantity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxQuantity()
    {
        return $this->maxQuantity;
    }

    /**
     * @param float $maxQuantity
     * @return Product
     */
    public function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getLifecycle()
    {
        return $this->lifecycle;
    }

    /**
     * @param string $lifecycle
     * @return Product
     */
    public function setLifecycle($lifecycle)
    {
        $this->lifecycle = $lifecycle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerialControlFlag()
    {
        return $this->serialControlFlag;
    }

    /**
     * @param string $serialControlFlag
     * @return Product
     */
    public function setSerialControlFlag($serialControlFlag)
    {
        $this->serialControlFlag = $serialControlFlag;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerialStartString()
    {
        return $this->serialStartString;
    }

    /**
     * @param string $serialStartString
     * @return Product
     */
    public function setSerialStartString($serialStartString)
    {
        $this->serialStartString = $serialStartString;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerialEndString()
    {
        return $this->serialEndString;
    }

    /**
     * @param string|null $serialEndString
     * @return Product
     */
    public function setSerialEndString($serialEndString)
    {
        $this->serialEndString = $serialEndString;
        return $this;
    }

    /**
     * @return float
     */
    public function getSerialLengthMax()
    {
        return $this->serialLengthMax;
    }

    /**
     * @param float $serialLengthMax
     * @return Product
     */
    public function setSerialLengthMax($serialLengthMax)
    {
        $this->serialLengthMax = $serialLengthMax;
        return $this;
    }

    /**
     * @return float
     */
    public function getSerialLengthMin()
    {
        return $this->serialLengthMin;
    }

    /**
     * @param float $serialLengthMin
     * @return Product
     */
    public function setSerialLengthMin($serialLengthMin)
    {
        $this->serialLengthMin = $serialLengthMin;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     * @return Product
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVendorPartNumber()
    {
        return $this->vendorPartNumber;
    }

    /**
     * @param string $vendorPartNumber
     * @return Product
     */
    public function setVendorPartNumber($vendorPartNumber)
    {
        $this->vendorPartNumber = $vendorPartNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * @param string $upc
     * @return Product
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpcAlternate()
    {
        return $this->upcAlternate;
    }

    /**
     * @param string $upcAlternate
     * @return Product
     */
    public function setUpcAlternate($upcAlternate)
    {
        $this->upcAlternate = $upcAlternate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCbsItemTemplate()
    {
        return $this->cbsItemTemplate;
    }

    /**
     * @param string $cbsItemTemplate
     * @return Product
     */
    public function setCbsItemTemplate($cbsItemTemplate)
    {
        $this->cbsItemTemplate = $cbsItemTemplate;
        return $this;
    }

    /**
     * @param string $pricingTemplate
     * @return Product
     */
    public function setPricingTemplate($pricingTemplate)
    {
        $this->pricingTemplate = $pricingTemplate;
        return $this;
    }

    /**
     * @return string
     */
    public function getPricingTemplate()
    {
        return $this->pricingTemplate;
    }

    /**
     * @return float
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * @param float $basePrice
     * @return Product
     */
    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Product
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return float
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * @param float $costCenter
     * @return Product
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlannerCode()
    {
        return $this->plannerCode;
    }

    /**
     * @param string $plannerCode
     * @return Product
     */
    public function setPlannerCode($plannerCode)
    {
        $this->plannerCode = $plannerCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getMinMaxActive()
    {
        return $this->minMaxActive;
    }

    /**
     * @param string $minMaxActive
     * @return Product
     */
    public function setMinMaxActive($minMaxActive)
    {
        $this->minMaxActive = $minMaxActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param string $buyer
     * @return Product
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductManager()
    {
        return $this->productManager;
    }

    /**
     * @param string $productManager
     * @return Product
     */
    public function setProductManager($productManager)
    {
        $this->productManager = $productManager;
        return $this;
    }

    /**
     * @return string
     */
    public function getInventoryControlType()
    {
        return $this->inventoryControlType;
    }

    /**
     * @param string $inventoryControlType
     * @return Product
     */
    public function setInventoryControlType($inventoryControlType)
    {
        $this->inventoryControlType = $inventoryControlType;
        return $this;
    }

    /**
     * @return float
     */
    public function getFixedLotMultiplier()
    {
        return $this->fixedLotMultiplier;
    }

    /**
     * @param float $fixedLotMultiplier
     * @return Product
     */
    public function setFixedLotMultiplier($fixedLotMultiplier)
    {
        $this->fixedLotMultiplier = $fixedLotMultiplier;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalesPoint()
    {
        return $this->salesPoint;
    }

    /**
     * @param string $salesPoint
     * @return Product
     */
    public function setSalesPoint($salesPoint)
    {
        $this->salesPoint = $salesPoint;
        return $this;
    }

    /**
     * @return string
     */
    public function getPsPrimaryLocator()
    {
        return $this->psPrimaryLocator;
    }

    /**
     * @param string $psPrimaryLocator
     * @return Product
     */
    public function setPsPrimaryLocator($psPrimaryLocator)
    {
        $this->psPrimaryLocator = $psPrimaryLocator;
        return $this;
    }

    /**
     * @return string
     */
    public function getPsSecondaryLocator()
    {
        return $this->psSecondaryLocator;
    }

    /**
     * @param string $psSecondaryLocator
     * @return Product
     */
    public function setPsSecondaryLocator($psSecondaryLocator)
    {
        $this->psSecondaryLocator = $psSecondaryLocator;
        return $this;
    }

    /**
     * @return float
     */
    public function getPsMin()
    {
        return $this->psMin;
    }

    /**
     * @param float $psMin
     * @return Product
     */
    public function setPsMin($psMin)
    {
        $this->psMin = $psMin;
        return $this;
    }

    /**
     * @return float
     */
    public function getPsMax()
    {
        return $this->psMax;
    }

    /**
     * @param float $psMax
     * @return Product
     */
    public function setPsMax($psMax)
    {
        $this->psMax = $psMax;
        return $this;
    }

    /**
     * @return string
     */
    public function getHslcPrimaryLocator()
    {
        return $this->hslcPrimaryLocator;
    }

    /**
     * @param string $hslcPrimaryLocator
     * @return Product
     */
    public function setHslcPrimaryLocator($hslcPrimaryLocator)
    {
        $this->hslcPrimaryLocator = $hslcPrimaryLocator;
        return $this;
    }

    /**
     * @return string
     */
    public function getHslcSecondaryLocator()
    {
        return $this->hslcSecondaryLocator;
    }

    /**
     * @param string $hslcSecondaryLocator
     * @return Product
     */
    public function setHslcSecondaryLocator($hslcSecondaryLocator)
    {
        $this->hslcSecondaryLocator = $hslcSecondaryLocator;
        return $this;
    }

    /**
     * @return float
     */
    public function getHslcMin()
    {
        return $this->hslcMin;
    }

    /**
     * @param float $hslcMin
     * @return Product
     */
    public function setHslcMin($hslcMin)
    {
        $this->hslcMin = $hslcMin;
        return $this;
    }

    /**
     * @return float
     */
    public function getHslcMax()
    {
        return $this->hslcMax;
    }

    /**
     * @param float $hslcMax
     * @return Product
     */
    public function setHslcMax($hslcMax)
    {
        $this->hslcMax = $hslcMax;
        return $this;
    }

    /**
     * @return string
     */
    public function getEcmPrimaryLocator()
    {
        return $this->ecmPrimaryLocator;
    }

    /**
     * @param string $ecmPrimaryLocator
     * @return Product
     */
    public function setEcmPrimaryLocator($ecmPrimaryLocator)
    {
        $this->ecmPrimaryLocator = $ecmPrimaryLocator;
        return $this;
    }

    /**
     * @return string
     */
    public function getEcmSecondaryLocator()
    {
        return $this->ecmSecondaryLocator;
    }

    /**
     * @param string $ecmSecondaryLocator
     * @return Product
     */
    public function setEcmSecondaryLocator($ecmSecondaryLocator)
    {
        $this->ecmSecondaryLocator = $ecmSecondaryLocator;
        return $this;
    }

    /**
     * @return float
     */
    public function getEcmMin()
    {
        return $this->ecmMin;
    }

    /**
     * @param float $ecmMin
     * @return Product
     */
    public function setEcmMin($ecmMin)
    {
        $this->ecmMin = $ecmMin;
        return $this;
    }

    /**
     * @return float
     */
    public function getEcmMax()
    {
        return $this->ecmMax;
    }

    /**
     * @param float $ecmMax
     * @return Product
     */
    public function setEcmMax($ecmMax)
    {
        $this->ecmMax = $ecmMax;
        return $this;
    }

    /**
     * @return string
     */
    public function getDepEligible()
    {
        return $this->depEligible;
    }

    /**
     * @param string $depEligible
     * @return Product
     */
    public function setDepEligible($depEligible)
    {
        $this->depEligible = $depEligible;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddSalesInstructions()
    {
        return $this->addSalesInstructions;
    }

    /**
     * @param string $addSalesInstructions
     * @return Product
     */
    public function setAddSalesInstructions($addSalesInstructions)
    {
        $this->addSalesInstructions = $addSalesInstructions;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalesProcedure()
    {
        return $this->salesProcedure;
    }

    /**
     * @param string $salesProcedure
     * @return Product
     */
    public function setSalesProcedure($salesProcedure)
    {
        $this->salesProcedure = $salesProcedure;
        return $this;
    }

    /**
     * @return string
     */
    public function getTestMode()
    {
        return $this->testMode;
    }

    /**
     * @param string $testMode
     * @return Product
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function __toString()
    {
        $reflect = new \ReflectionClass($this);
        $str = '';

        foreach ($reflect->getProperties() as $prop) {
            $prop->setAccessible(true);
            $str .= $prop->getName() . ': ' . $prop->getValue($this) . ' ';
        }

        return $str;
    }

}
