<?php

namespace edu\wisc\services\cbs\product;

use edu\wisc\services\cbs\api\Service;
use edu\wisc\services\cbs\exception\ProductServiceCreateException;
use edu\wisc\services\cbs\exception\ProductServiceUpdateException;
use edu\wisc\services\cbs\exception\UnsupportedCurrencyException;

/**
 * Service interface for managing products.
 *
 * @author Nicholas Blair
 */
interface ProductService extends Service
{

    /**
     * Create a new product.
     *
     * @param Product product to create
     * @return ProductServiceResponse
     * @throws UnsupportedCurrencyException
     * @throws ProductServiceCreateException
     */
    public function createProduct(Product $product): ProductServiceResponse;

    /**
     * Update an existing product.
     *
     * @param Product product to update
     * @return ProductServiceResponse
     * @throws UnsupportedCurrencyException
     * @throws ProductServiceUpdateException
     */
    public function updateProduct(Product $product): ProductServiceResponse;

}
