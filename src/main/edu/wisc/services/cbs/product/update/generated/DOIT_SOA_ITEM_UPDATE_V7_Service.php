<?php

namespace edu\wisc\services\cbs\product\update\generated;

class DOIT_SOA_ITEM_UPDATE_V7_Service extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'InputParameters' => 'edu\\wisc\\services\\cbs\\product\\update\\generated\\InputParameters',
      'OutputParameters' => 'edu\\wisc\\services\\cbs\\product\\update\\generated\\OutputParameters',
      'SOAHeader' => 'edu\\wisc\\services\\cbs\\product\\update\\generated\\SOAHeader',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'src/main/resources/doit_soa_item_update_v7.xml';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param InputParameters $body
     * @return OutputParameters
     */
    public function ITEM_UPDATE(InputParameters $body)
    {
      return $this->__soapCall('ITEM_UPDATE', array($body));
    }

}
