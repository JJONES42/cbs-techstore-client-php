<?php


 function autoload_dfe7eff50c2a7962644606a394338e3b($class)
{
    $classes = array(
        'edu\wisc\services\cbs\product\update\generated\DOIT_SOA_ITEM_UPDATE_V7_Service' => __DIR__ .'/DOIT_SOA_ITEM_UPDATE_V7_Service.php',
        'edu\wisc\services\cbs\product\update\generated\InputParameters' => __DIR__ .'/InputParameters.php',
        'edu\wisc\services\cbs\product\update\generated\OutputParameters' => __DIR__ .'/OutputParameters.php',
        'edu\wisc\services\cbs\product\update\generated\SOAHeader' => __DIR__ .'/SOAHeader.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_dfe7eff50c2a7962644606a394338e3b');

// Do nothing. The rest is just leftovers from the code generation.
{
}
