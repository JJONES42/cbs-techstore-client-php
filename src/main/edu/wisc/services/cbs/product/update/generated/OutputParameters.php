<?php

namespace edu\wisc\services\cbs\product\update\generated;

class OutputParameters
{

    /**
     * @var string $P_STATUS
     */
    protected $P_STATUS = null;

    /**
     * @var string $P_RESULT_MESSAGE
     */
    protected $P_RESULT_MESSAGE = null;

    /**
     * @var string $P_ITEM_NO
     */
    protected $P_ITEM_NO = null;

    /**
     * @var float $P_ITEM_ID
     */
    protected $P_ITEM_ID = null;

    /**
     * @param string $P_STATUS
     * @param string $P_RESULT_MESSAGE
     * @param string $P_ITEM_NO
     * @param float $P_ITEM_ID
     */
    public function __construct($P_STATUS, $P_RESULT_MESSAGE, $P_ITEM_NO, $P_ITEM_ID)
    {
      $this->P_STATUS = $P_STATUS;
      $this->P_RESULT_MESSAGE = $P_RESULT_MESSAGE;
      $this->P_ITEM_NO = $P_ITEM_NO;
      $this->P_ITEM_ID = $P_ITEM_ID;
    }

    /**
     * @return string
     */
    public function getP_STATUS()
    {
      return $this->P_STATUS;
    }

    /**
     * @param string $P_STATUS
     * @return \edu\wisc\services\cbs\product\update\generated\OutputParameters
     */
    public function setP_STATUS($P_STATUS)
    {
      $this->P_STATUS = $P_STATUS;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_RESULT_MESSAGE()
    {
      return $this->P_RESULT_MESSAGE;
    }

    /**
     * @param string $P_RESULT_MESSAGE
     * @return \edu\wisc\services\cbs\product\update\generated\OutputParameters
     */
    public function setP_RESULT_MESSAGE($P_RESULT_MESSAGE)
    {
      $this->P_RESULT_MESSAGE = $P_RESULT_MESSAGE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_ITEM_NO()
    {
      return $this->P_ITEM_NO;
    }

    /**
     * @param string $P_ITEM_NO
     * @return \edu\wisc\services\cbs\product\update\generated\OutputParameters
     */
    public function setP_ITEM_NO($P_ITEM_NO)
    {
      $this->P_ITEM_NO = $P_ITEM_NO;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_ITEM_ID()
    {
      return $this->P_ITEM_ID;
    }

    /**
     * @param float $P_ITEM_ID
     * @return \edu\wisc\services\cbs\product\update\generated\OutputParameters
     */
    public function setP_ITEM_ID($P_ITEM_ID)
    {
      $this->P_ITEM_ID = $P_ITEM_ID;
      return $this;
    }

}
