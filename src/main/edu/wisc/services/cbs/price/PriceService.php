<?php

namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\api\Service;
use edu\wisc\services\cbs\exception\PriceServiceException;
use edu\wisc\services\cbs\exception\UnsupportedCurrencyException;
use Money\Money;

/**
 * Service intended for managing product prices.
 */
interface PriceService extends Service
{

    /**
     * Update the price for the given SKU.
     *
     * @param string $sku
     * @param float $price
     * @return PriceServiceResponse
     * @throws UnsupportedCurrencyException
     * @throws PriceServiceException
     */
    public function updateProductPrice(string $sku, float $price): PriceServiceResponse;

}
