<?php
namespace edu\wisc\services\cbs\price;

use edu\wisc\services\cbs\api\SoapService;
use edu\wisc\services\cbs\common\MoneyConversion;
use edu\wisc\services\cbs\common\WssSoapHeaderBuilder;
use edu\wisc\services\cbs\exception\PriceServiceException;
use edu\wisc\services\cbs\exception\UnsupportedCurrencyException;
use edu\wisc\services\cbs\price\generated\DOIT_SOA_PRICING_V2_Service;
use edu\wisc\services\cbs\price\generated\InputParameters;
use Money\Money;

/**
 * A SOAP-backed implementation of {@link ProductPriceService}.
 */
class SoapPriceService implements PriceService, SoapService
{

    /** URL for QA WSLD */
    const CBQA12 = 'http://pegasus.doit.wisc.edu:8018/webservices/SOAProvider/plsql/doit_soa_pricing_v2/?wsdl';
    /** URL for DV WSDL */
    const CBDV12 = 'http://pegasus.doit.wisc.edu:8016/webservices/SOAProvider/plsql/doit_soa_pricing_v2/?wsdl';
    /** URL for CP WSDL */
    const CBCP12 = 'http://pegasus.doit.wisc.edu:8015/webservices/SOAProvider/plsql/doit_soa_pricing_v2/?wsdl';
    /** URL for PROD WSDL */
    const CBSP = 'http://galactica.doit.wisc.edu:8001/webservices/SOAProvider/plsql/doit_soa_pricing_v2/?wsdl';

    /** @var \SoapClient */
    private $soapClient;

    /**
     * @inheritdoc
     */
    public function __construct($username, $password, $wsdlPath = null, \SoapClient $priceSoapClient = null)
    {
        if ($priceSoapClient !== null) {
            $this->soapClient = $priceSoapClient;
            return;
        } else if ($wsdlPath !== null) {
            $this->soapClient = new DOIT_SOA_PRICING_V2_Service(
                [],
                $wsdlPath
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        } else {
            $this->soapClient = new DOIT_SOA_PRICING_V2_Service(
                [],
                __DIR__ . '/../../../../../resources/doit_soa_pricing_v2.xml'
            );
            $this->soapClient->__setSoapHeaders(WssSoapHeaderBuilder::buildUsernameToken($username, $password));
        }
    }

    /**
     * @inheritdoc
     */
    public function updateProductPrice(string $sku, float $price): PriceServiceResponse
    {
        try {
            $outputParameters = $this->soapClient->ITEM_PRICING(
                new InputParameters(
                    $sku,
                    $price,
                    '',
                    '',
                    'TBD'
                )
            );
            return new PriceServiceResponse(
                strcasecmp($outputParameters->getP_STATUS(), 'SUCCESS') === 0,
                $outputParameters->getP_RESULT_MESSAGE()
            );
        } catch (\Exception $e) {
            throw new PriceServiceException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
