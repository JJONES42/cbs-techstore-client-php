<?php

namespace edu\wisc\services\cbs\price\generated;

class InputParameters
{

    /**
     * @var string $P_ITEM_NUMBER
     */
    protected $P_ITEM_NUMBER = null;

    /**
     * @var float $P_GLOBAL_PRICE
     */
    protected $P_GLOBAL_PRICE = null;

    /**
     * @var string $P_WEBSITE_PRICING
     */
    protected $P_WEBSITE_PRICING = null;

    /**
     * @var string $P_REFERENCE
     */
    protected $P_REFERENCE = null;

    /**
     * @var string $P_TEST_MODE
     */
    protected $P_TEST_MODE = null;

    /**
     * @param string $P_ITEM_NUMBER
     * @param float $P_GLOBAL_PRICE
     * @param string $P_WEBSITE_PRICING
     * @param string $P_REFERENCE
     * @param string $P_TEST_MODE
     */
    public function __construct($P_ITEM_NUMBER, $P_GLOBAL_PRICE, $P_WEBSITE_PRICING, $P_REFERENCE, $P_TEST_MODE)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      $this->P_GLOBAL_PRICE = $P_GLOBAL_PRICE;
      $this->P_WEBSITE_PRICING = $P_WEBSITE_PRICING;
      $this->P_REFERENCE = $P_REFERENCE;
      $this->P_TEST_MODE = $P_TEST_MODE;
    }

    /**
     * @return string
     */
    public function getP_ITEM_NUMBER()
    {
      return $this->P_ITEM_NUMBER;
    }

    /**
     * @param string $P_ITEM_NUMBER
     * @return \edu\wisc\services\cbs\price\generated\InputParameters
     */
    public function setP_ITEM_NUMBER($P_ITEM_NUMBER)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_GLOBAL_PRICE()
    {
      return $this->P_GLOBAL_PRICE;
    }

    /**
     * @param float $P_GLOBAL_PRICE
     * @return \edu\wisc\services\cbs\price\generated\InputParameters
     */
    public function setP_GLOBAL_PRICE($P_GLOBAL_PRICE)
    {
      $this->P_GLOBAL_PRICE = $P_GLOBAL_PRICE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_WEBSITE_PRICING()
    {
      return $this->P_WEBSITE_PRICING;
    }

    /**
     * @param string $P_WEBSITE_PRICING
     * @return \edu\wisc\services\cbs\price\generated\InputParameters
     */
    public function setP_WEBSITE_PRICING($P_WEBSITE_PRICING)
    {
      $this->P_WEBSITE_PRICING = $P_WEBSITE_PRICING;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_REFERENCE()
    {
      return $this->P_REFERENCE;
    }

    /**
     * @param string $P_REFERENCE
     * @return \edu\wisc\services\cbs\price\generated\InputParameters
     */
    public function setP_REFERENCE($P_REFERENCE)
    {
      $this->P_REFERENCE = $P_REFERENCE;
      return $this;
    }

    /**
     * @return string
     */
    public function getP_TEST_MODE()
    {
      return $this->P_TEST_MODE;
    }

    /**
     * @param string $P_TEST_MODE
     * @return \edu\wisc\services\cbs\price\generated\InputParameters
     */
    public function setP_TEST_MODE($P_TEST_MODE)
    {
      $this->P_TEST_MODE = $P_TEST_MODE;
      return $this;
    }

}
