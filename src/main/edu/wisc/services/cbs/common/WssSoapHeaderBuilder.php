<?php
namespace edu\wisc\services\cbs\common;

/**
 * Builds a WS-Security header to be used by a {@link \SoapClient}.
 *
 * @see https://msdn.microsoft.com/en-us/library/ms977327.aspx
 */
class WssSoapHeaderBuilder
{

    const WSS_NAMESPACE = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    /**
     * Returns a SOAP header that supports UsernameToken.
     *
     * The returned object should be added to a SOAP client using {@link \SoapClient::__setSoapHeaders}.
     *
     * @param string $username
     * @param string $password
     * @return \SOAPHeader
     */
    public static function buildUsernameToken($username, $password)
    {
        $token = new \stdClass;
        $token->Username = new \SoapVar($username, XSD_STRING, null, null, null, static::WSS_NAMESPACE);
        $token->Password = new \SoapVar($password, XSD_STRING, null, null, null, static::WSS_NAMESPACE);
        $wsec = new \stdClass;
        $wsec->UsernameToken = new \SoapVar($token, SOAP_ENC_OBJECT, null, null, null, static::WSS_NAMESPACE);
        return new \SOAPHeader(static::WSS_NAMESPACE, 'Security', $wsec, true);
    }
}
