<?php
namespace edu\wisc\services\cbs\api;

/**
 * A mock implementation of a CBS service.
 */
interface MockService extends Service
{

    /**
     * Creates a mock implementation of a {@link Service}.
     *
     * Service will respond with the given {@link ServiceResponseInterface}, or a successful response if one is not
     * given.
     * @param bool $success
     */
    public function __construct(bool $success = true);
}
