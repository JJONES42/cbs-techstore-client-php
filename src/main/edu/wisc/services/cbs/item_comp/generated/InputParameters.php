<?php

namespace edu\wisc\services\cbs\item_comp\generated;

class InputParameters
{

    /**
     * @var string $P_ITEM_NUMBER
     */
    protected $P_ITEM_NUMBER = null;

    /**
     * @var float $P_INVENTORY_ITEM_ID
     */
    protected $P_INVENTORY_ITEM_ID = null;

    /**
     * @param string $P_ITEM_NUMBER
     * @param float $P_INVENTORY_ITEM_ID
     */
    public function __construct($P_ITEM_NUMBER, $P_INVENTORY_ITEM_ID)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      $this->P_INVENTORY_ITEM_ID = $P_INVENTORY_ITEM_ID;
    }

    /**
     * @return string
     */
    public function getP_ITEM_NUMBER()
    {
      return $this->P_ITEM_NUMBER;
    }

    /**
     * @param string $P_ITEM_NUMBER
     * @return \edu\wisc\services\cbs\item_comp\generated\InputParameters
     */
    public function setP_ITEM_NUMBER($P_ITEM_NUMBER)
    {
      $this->P_ITEM_NUMBER = $P_ITEM_NUMBER;
      return $this;
    }

    /**
     * @return float
     */
    public function getP_INVENTORY_ITEM_ID()
    {
      return $this->P_INVENTORY_ITEM_ID;
    }

    /**
     * @param float $P_INVENTORY_ITEM_ID
     * @return \edu\wisc\services\cbs\item_comp\generated\InputParameters
     */
    public function setP_INVENTORY_ITEM_ID($P_INVENTORY_ITEM_ID)
    {
      $this->P_INVENTORY_ITEM_ID = $P_INVENTORY_ITEM_ID;
      return $this;
    }

}
