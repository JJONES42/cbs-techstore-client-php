<?php

namespace edu\wisc\services\cbs\item_comp;

/**
 * ItemCompareService compares the attributes for a product in Magento with CBS.
 */
interface ItemCompareService
{

    /**
     * Compare an item from Magento with an item in CBS
     * @param string $sku
     * @return ItemCompareServiceResponse
     */
    public function compareItemWithCbs($sku);

}