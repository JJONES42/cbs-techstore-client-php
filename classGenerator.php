<?php

/**
 * This script generates classes to make SOAP calls for a given WSDL.
 *
 * WSDLs belong in: src/main/resources
 * Generated classes belong in: src/main/edu/wisc/services/cbs/[service]/generated
 *
 * Usage: php classGenerator.php [WSDL] [Output Directory] [Namespace]
 */

use Monolog\Logger;

require_once(__DIR__ . '/vendor/autoload.php');

if ($argc !== 4) {
    echo "Usage: php classGenerator.php [WSDL] [Output Directory] [Namespace]\n";
    exit(1);
}

list(, $wsdl, $outputDir, $namespace) = $argv;

// Delete existing files
$files = glob($outputDir . '/*');
foreach ($files as $file) {
    if (is_file($file)) {
        unlink($file);
    }
}
@rmdir($outputDir);

$logger = new Logger('Wsdl2PhpLog');
$logger->pushHandler(new \Monolog\Handler\StreamHandler(STDOUT));

$generator = new \Wsdl2PhpGenerator\Generator();
$generator->setLogger($logger);
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => $wsdl,
        'outputDir' => $outputDir,
        'namespaceName' => $namespace
    ))
);
